EESchema Schematic File Version 4
EELAYER 30 0
EELAYER END
$Descr User 17111 11668
encoding utf-8
Sheet 1 1
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L Adafruit-2.13in-Tri-Color-eInk-Display-rescue:FRAME_A3-Adafruit_2.13in_Tri-Color_eInk_Display-eagle-import #FRAME1
U 1 1 73DA64CA
P 900 10800
F 0 "#FRAME1" H 900 10800 50  0001 C CNN
F 1 "FRAME_A3" H 900 10800 50  0001 C CNN
F 2 "" H 900 10800 50  0001 C CNN
F 3 "" H 900 10800 50  0001 C CNN
	1    900  10800
	1    0    0    -1  
$EndComp
$Comp
L Adafruit-2.13in-Tri-Color-eInk-Display-rescue:FRAME_A3-Adafruit_2.13in_Tri-Color_eInk_Display-eagle-import #FRAME1
U 3 1 73DA64C2
P 11200 10700
F 0 "#FRAME1" H 11200 10700 50  0001 C CNN
F 1 "FRAME_A3" H 11200 10700 50  0001 C CNN
F 2 "" H 11200 10700 50  0001 C CNN
F 3 "" H 11200 10700 50  0001 C CNN
	3    11200 10700
	1    0    0    -1  
$EndComp
$Comp
L Adafruit-2.13in-Tri-Color-eInk-Display-rescue:MOUNTINGHOLE2.5-Adafruit_2.13in_Tri-Color_eInk_Display-eagle-import U$3
U 1 1 BFAA01A9
P 15800 9100
F 0 "U$3" H 15800 9100 50  0001 C CNN
F 1 "MOUNTINGHOLE2.5" H 15800 9100 50  0001 C CNN
F 2 "Adafruit 2.13in Tri-Color eInk Display:MOUNTINGHOLE_2.5_PLATED" H 15800 9100 50  0001 C CNN
F 3 "" H 15800 9100 50  0001 C CNN
	1    15800 9100
	1    0    0    -1  
$EndComp
$Comp
L Adafruit-2.13in-Tri-Color-eInk-Display-rescue:MOUNTINGHOLE2.5-Adafruit_2.13in_Tri-Color_eInk_Display-eagle-import U$4
U 1 1 6F721AB2
P 15500 9100
F 0 "U$4" H 15500 9100 50  0001 C CNN
F 1 "MOUNTINGHOLE2.5" H 15500 9100 50  0001 C CNN
F 2 "Adafruit 2.13in Tri-Color eInk Display:MOUNTINGHOLE_2.5_PLATED" H 15500 9100 50  0001 C CNN
F 3 "" H 15500 9100 50  0001 C CNN
	1    15500 9100
	1    0    0    -1  
$EndComp
$Comp
L Adafruit-2.13in-Tri-Color-eInk-Display-rescue:MOUNTINGHOLE2.5-Adafruit_2.13in_Tri-Color_eInk_Display-eagle-import U$5
U 1 1 81F2EEA7
P 15200 9100
F 0 "U$5" H 15200 9100 50  0001 C CNN
F 1 "MOUNTINGHOLE2.5" H 15200 9100 50  0001 C CNN
F 2 "Adafruit 2.13in Tri-Color eInk Display:MOUNTINGHOLE_2.5_PLATED" H 15200 9100 50  0001 C CNN
F 3 "" H 15200 9100 50  0001 C CNN
	1    15200 9100
	1    0    0    -1  
$EndComp
$Comp
L Adafruit-2.13in-Tri-Color-eInk-Display-rescue:MOUNTINGHOLE2.5-Adafruit_2.13in_Tri-Color_eInk_Display-eagle-import U$6
U 1 1 DC862BA9
P 14900 9100
F 0 "U$6" H 14900 9100 50  0001 C CNN
F 1 "MOUNTINGHOLE2.5" H 14900 9100 50  0001 C CNN
F 2 "Adafruit 2.13in Tri-Color eInk Display:MOUNTINGHOLE_2.5_PLATED" H 14900 9100 50  0001 C CNN
F 3 "" H 14900 9100 50  0001 C CNN
	1    14900 9100
	1    0    0    -1  
$EndComp
$Comp
L Adafruit-2.13in-Tri-Color-eInk-Display-rescue:FIDUCIAL_1MM-Adafruit_2.13in_Tri-Color_eInk_Display-eagle-import FID1
U 1 1 DB63CB8D
P 14400 9100
F 0 "FID1" H 14400 9100 50  0001 C CNN
F 1 "FIDUCIAL_1MM" H 14400 9100 50  0001 C CNN
F 2 "Adafruit 2.13in Tri-Color eInk Display:FIDUCIAL_1MM" H 14400 9100 50  0001 C CNN
F 3 "" H 14400 9100 50  0001 C CNN
	1    14400 9100
	1    0    0    -1  
$EndComp
$Comp
L Adafruit-2.13in-Tri-Color-eInk-Display-rescue:FIDUCIAL_1MM-Adafruit_2.13in_Tri-Color_eInk_Display-eagle-import FID2
U 1 1 E067070F
P 14600 9100
F 0 "FID2" H 14600 9100 50  0001 C CNN
F 1 "FIDUCIAL_1MM" H 14600 9100 50  0001 C CNN
F 2 "Adafruit 2.13in Tri-Color eInk Display:FIDUCIAL_1MM" H 14600 9100 50  0001 C CNN
F 3 "" H 14600 9100 50  0001 C CNN
	1    14600 9100
	1    0    0    -1  
$EndComp
$Comp
L Adafruit-2.13in-Tri-Color-eInk-Display-rescue:FIDUCIAL_1MM-Adafruit_2.13in_Tri-Color_eInk_Display-eagle-import FID3
U 1 1 4801E4DA
P 14200 9100
F 0 "FID3" H 14200 9100 50  0001 C CNN
F 1 "FIDUCIAL_1MM" H 14200 9100 50  0001 C CNN
F 2 "Adafruit 2.13in Tri-Color eInk Display:FIDUCIAL_1MM" H 14200 9100 50  0001 C CNN
F 3 "" H 14200 9100 50  0001 C CNN
	1    14200 9100
	1    0    0    -1  
$EndComp
Text Notes 12500 9900 0    85   ~ 0
Pervasive Displays 2.0" Breakout
Wire Notes Line
	16000 3000 11200 3000
Wire Notes Line
	16000 8900 11200 8900
Wire Notes Line
	10450 625  10450 9225
$Comp
L Adafruit-2.13in-Tri-Color-eInk-Display-rescue:EINK_EPD0231-Adafruit_2.13in_Tri-Color_eInk_Display-eagle-import EINK1
U 1 1 D2324609
P 10400 5900
F 0 "EINK1" H 10000 7600 42  0000 L BNN
F 1 "EINK_EPD0231" H 10000 4200 42  0000 L BNN
F 2 "Adafruit 2.13in Tri-Color eInk Display:EINK_213IN_104X212" H 10400 5900 50  0001 C CNN
F 3 "" H 10400 5900 50  0001 C CNN
	1    10400 5900
	1    0    0    -1  
$EndComp
Text Label 9900 6200 0    10   ~ 0
3.3V
$EndSCHEMATC
