EESchema Schematic File Version 4
EELAYER 30 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 1 1
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L KICAD_Meadow_EDA:MEADOWF7MICRO_WITH_OUTLINE J1
U 1 1 659055A5
P 5000 3100
F 0 "J1" H 5800 3747 50  0000 C CNN
F 1 "MEADOWF7MICRO_WITH_OUTLINE" H 5800 3656 50  0000 C CNN
F 2 "KICAD_Meadow_EDA:MeadowF7Micro_w_outline" H 5800 1250 50  0001 C CNN
F 3 "https://store.wildernesslabs.co/collections/frontpage/products/meadow-f7" H 5600 1100 50  0001 C CNN
F 4 "Wilderness Labs" H 5800 3565 50  0000 C CNN "MF"
F 5 "Meadow F7 Micro " H 5800 3474 50  0000 C CNN "PN"
	1    5000 3100
	1    0    0    -1  
$EndComp
$Comp
L Device:Jumper_NO_Small JP1
U 1 1 6595A870
P 6900 3300
F 0 "JP1" H 6900 3485 50  0000 C CNN
F 1 "Jumper_NO_Small" H 6900 3394 50  0000 C CNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_1x02_P2.54mm_Vertical" H 6900 3300 50  0001 C CNN
F 3 "~" H 6900 3300 50  0001 C CNN
	1    6900 3300
	1    0    0    -1  
$EndComp
Wire Wire Line
	6600 3300 6800 3300
$Comp
L Displays:Adafruit_2.13in_250x122_Epaper U1
U 1 1 659612BF
P 8550 4050
F 0 "U1" V 7989 4069 50  0000 L CNN
F 1 "Adafruit_2.13in_250x122_Epaper" H 7925 4250 50  0000 L CNN
F 2 "Displays:Adafruit_2.16in_250x122_Epaper" H 8550 4050 50  0001 C CNN
F 3 "" H 8550 4050 50  0001 C CNN
	1    8550 4050
	0    1    1    0   
$EndComp
$Comp
L Device:Battery_Cell BT1
U 1 1 6596B75F
P 7525 3300
F 0 "BT1" V 7780 3350 50  0000 C CNN
F 1 "Battery_Cell" V 7689 3350 50  0000 C CNN
F 2 "Battery:BatteryHolder_Keystone_2462_2xAA" V 7525 3360 50  0001 C CNN
F 3 "~" V 7525 3360 50  0001 C CNN
	1    7525 3300
	0    -1   -1   0   
$EndComp
Wire Wire Line
	7000 3300 7325 3300
Wire Wire Line
	7625 3300 7800 3300
Text Label 7800 3300 0    50   ~ 0
GND
Wire Wire Line
	5000 3200 4825 3200
Text Label 4825 3200 0    50   ~ 0
GND
Wire Wire Line
	6600 3400 6900 3400
Wire Wire Line
	6900 3400 6900 3725
$Comp
L Switch:SW_DIP_x01 SW1
U 1 1 6598CEED
P 7475 3725
F 0 "SW1" H 7475 3992 50  0000 C CNN
F 1 "SW_DIP_x01" H 7475 3901 50  0000 C CNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_1x02_P2.54mm_Vertical" H 7475 3725 50  0001 C CNN
F 3 "~" H 7475 3725 50  0001 C CNN
	1    7475 3725
	1    0    0    -1  
$EndComp
Wire Wire Line
	6900 3725 7175 3725
Wire Wire Line
	7775 3725 7875 3725
Text Label 7875 3725 0    50   ~ 0
GND
$EndSCHEMATC
