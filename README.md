# Weather Willy

A simple weather station UI with personality

## Stories

- As a **user**, I want to be able to read a selected set of **information off the display**
- As a **user**, I want to see **weather willy change** attire depending on the weather
- As a **user**, I want to be **instructed** on how to setup my device
- As a **user**, I want to see both **indoor and outdoor** temperatures when looking at the display
- As a **user**, I want to be able to **configure** the application using a simple web interface

## Description

> This is a work in progress!


## Roadmap

See **[vision statement]** and **[milstones]**

for feature information

[weather.com]: weather.com
[weatherapi]: https://www.weatherapi.com/
[dknoodle/WUnderground.Net]: https://github.com/dknoodle/WUnderground.Net
[openweathermap]: https://openweathermap.org/api
[Meadow F7]: https://developer.wildernesslabs.co/Meadow/Meadow_Basics/Hardware/
[Waveshare E-Ink Display]: https://www.waveshare.com/2.9inch-e-Paper-Module.htm
[wunderground]: https://www.wunderground.com/weather/api
[vision statement]: /docs/01_VISION.md