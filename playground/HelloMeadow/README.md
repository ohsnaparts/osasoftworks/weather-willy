# Hello Meadow

This projet focuses on a minimalistic deployable application
to validate the microcontroller is working as expected

## Technologies

- Meadow Hello World
- Manual Meadow deployment
- Handling meadow with Powershell
- Toggling LEDs

## Running

```pwsh
./run.ps1
```

<!-- 
https://github.com/nbedos/termtosvg 
termtosvg --template window_frame_powershell
-->
![gif showing a full build and deployment run](./screenshots/termtosvg_by9kngow.svg)
