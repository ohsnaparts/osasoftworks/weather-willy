$ScriptsDirPath = Join-Path $PSScriptRoot 'scripts'
$BuildConfiguration='Debug'.ToLower()
$BuildsDirPath = Join-Path $PSScriptRoot "bin/$BuildConfiguration/artifacts"
$ApplicationMainDll = 'App.dll'
$SuccessErrorCode = 0

. (Join-Path $ScriptsDirPath 'Invoke-MeadowCommand.ps1')

$OutputColor = @{
    ForegroundColor = [System.ConsoleColor]::Green
}


Write-Host @OutputColor "Building application in $BuildConfiguration mode"
dotnet build --configuration $BuildConfiguration --output $BuildsDirPath
if($LASTEXITCODE -ne $SuccessErrorCode) {
    exit $LASTEXITCODE
}

Push-Location $BuildsDirPath
Try {
    Invoke-MeadowCommand -ErrorAction Stop -ScriptBlock {
        Write-Host @OutputColor "Deploying $ApplicationMainDll"
        meadow app deploy --file $ApplicationMainDll
    }
} Finally {
    Pop-Location
}

Write-Host @OutputColor "Listening to serial outputs..."
Invoke-MeadowCommand -ErrorAction Stop -ScriptBlock {
    meadow listen
}