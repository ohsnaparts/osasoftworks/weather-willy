using System;
using Meadow.Foundation;
using Meadow.Foundation.Graphics;
using Meadow.Logging;

namespace MeadowDesktopGtk.Display.DisplayElements;

public class CoordinateGridDisplayElement : DisplayElement
{
    public static int LineThickness { get; } = 1;
    public static IntVector CellPadding { get; } = new IntVector(1, 1);

    public IFont Font { get; private set; }

    public IntVector Size { get; private set; }
    public Color Color { get; private set; }

    public override IntVector BoundingBoxSize => this.Size;

    public override IntVector BoundingBoxPosition => this.Position;

    public IntVector MinBoundingBoxCoordinates => this.BoundingBoxPosition;
    public IntVector MaxBoundingBoxCoordinates => this.BoundingBoxPosition + this.BoundingBoxSize;

    public CoordinateGridDisplayElement(
        MicroGraphics graphics,
        IntVector position,
        IntVector size,
        Color color,
        IFont font,
        Logger logger,
        bool debugMode = false,
        bool supportsColor = false
    ) : base(graphics, position, logger, debugMode, supportsColor)
    {
        this.Color = color;
        this.Font = font;
        this.Size = size;
    }

    public override void Draw()
    {
        this._logger.Info($"Drawing {this.GetType().Name}");
        base.Draw();

        this.SetPenColor(this.Color);
        this.SetFont(this.Font);
        this.SetPenThickness(LineThickness);

        ForEachCell(cell =>
        {
            this._logger.Info(cell.ToString());
            DrawCellGrid(cell);
            DrawCellContent(cell);
        });
    }


    private void ForEachCell(Action<IntVector> action)
    {
        IntVector cellContentSize = this.getMaxCellContentSize();
        IntVector cellSize = cellContentSize + (2 * LineThickness);

        this._logger.Info($"Cell size: {cellSize}");
        this._logger.Info($"Cell content size: {cellContentSize}");
        WarnIfPotentiallyOverflowing(cellSize);

        for (int offsetY = 0; offsetY < this.Size.Y; offsetY += cellSize.Y)
        {
            for (int offsetX = 0; offsetX < this.Size.X; offsetX += cellSize.X)
            {
                action(this.Position + (offsetX, offsetY));
            }
        }
    }

    private void WarnIfPotentiallyOverflowing(IntVector cellSize)
    {
        var columnCount = this.Size.X / (float)cellSize.X;
        var rowCount = this.Size.Y / (float)cellSize.Y;
        
        var overflowingRowRatio = rowCount % 1;
        var overflowingColRatio = columnCount % 1;
        var missingPixels = this.Size * (overflowingRowRatio, overflowingColRatio);
        
        var underOrOverflowing = missingPixels.X != 0 || missingPixels.Y != 0;
        if (!underOrOverflowing)
        {
            return;
        }

        
        this._logger.Warn(
            $"""
            --------------------------------------------------------------------------
            The current element size might not fit all the cells fully. Presently 
            with your budget {this.Size}px, only {columnCount} columns and {rowCount}
            rows can be rendered fully onto the viewport. It is advised to set the size
            to a multiple of {cellSize}, otherwise there might be ugly incomplete 
            renderings. Try adjust the size by {missingPixels}.
            --------------------------------------------------------------------------
        """
        );
    }

    private void DrawCellGrid(IntVector cell)
    {
        // This drawing assumes a position of 0,0
        // 0,0   x/0
        //   ┌────┬────┐
        //   │    │    │
        // 0 │    │    │ max(x)
        // y ├────┼────┤   y
        //   │    │    │
        //   │    │    │
        //   └────┴────┘ max(x)
        //     x/max(y)  max(y)
        //

        _graphics.DrawLine(cell.X, this.Position.Y, cell.X, this.MaxBoundingBoxCoordinates.Y);
        _graphics.DrawLine(this.Position.X, cell.Y, this.MaxBoundingBoxCoordinates.X, cell.Y);
    }

    private void DrawCellContent(IntVector cell)
    {
        // +------------- lineThickness
        // | \ padding
        // |  + content
        // |
        IntVector cellContent = cell + CellPadding + LineThickness;
        _graphics.DrawText(
            cellContent.X,
            cellContent.Y,
            $"{cell.X:000}/{cell.Y:000}");
    }

    private IntVector getMaxCellContentSize()
    {
        int maxCoordinate = Math.Max(
            this.MaxBoundingBoxCoordinates.X,
            this.MaxBoundingBoxCoordinates.Y
        );
        var textWidth = this.Font.Width * $"{maxCoordinate}/{maxCoordinate}".Length;
        // top-bottom / left-right + content size
        return (CellPadding * 2) + (textWidth, this.Font.Height);
    }
}
