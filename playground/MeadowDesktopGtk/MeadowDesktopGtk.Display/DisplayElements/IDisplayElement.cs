using Meadow.Foundation;
using Meadow.Foundation.Graphics;

namespace MeadowDesktopGtk.Display.DisplayElements
{
    public interface IDisplayElement
    {
        public IntVector Position { get; }
        void Draw();
    }
}