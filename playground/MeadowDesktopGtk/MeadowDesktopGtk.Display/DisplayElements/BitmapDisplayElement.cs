using Meadow.Foundation.Graphics;
using Meadow.Logging;
using MeadowDesktopGtk.Display;
using MeadowDesktopGtk.Display.DisplayElements;
using MeadowDesktopGtk.Display.Extensions;

public class ImageDisplayElement : DisplayElement
{
    private readonly bool _treatWhiteAsTransparent;

    public ImageDisplayElement(
        MicroGraphics graphics,
        IntVector positionTopLeftOrigin,
        Image image,
        Logger logger,
        bool treatWhiteAsTransparent = true,
        bool debugMode = false,
        bool supportsColor = false
        ) : base(graphics, positionTopLeftOrigin, logger, debugMode, supportsColor)
    {
        Image = image;
        this._treatWhiteAsTransparent = treatWhiteAsTransparent;
    }

    public IntVector Size => new IntVector(
        this.Image.Width,
        this.Image.Height
    );

    public override IntVector BoundingBoxSize => Size + 10;

    public override IntVector BoundingBoxPosition => this.Position - 5;

    public Image Image { get; }

    public override void Draw()
    {
        base.Draw();

        if (!_treatWhiteAsTransparent)
        {
            this.DrawWhiteBackground();
        }

        this._graphics.DrawImage(this.Position, this.Image);
    }

    private void DrawWhiteBackground()
    {
        this._logger.Debug(
                "Drawing image ontop of white rectangle since the color white" +
                " is printed transparent by default"
            );

        // drawing the bitmap as-is renders colors as white
        // to work around that, we'll draw the bitmap pixel by pixel
        this.Image.Draw(this.Position, this._graphics);
        this.Image.PrintAsAscii(this._logger, true);
    }
}