using System;
using Meadow.Foundation;
using Meadow.Foundation.Graphics;
using Meadow.Logging;
using MeadowDesktopGtk.Display.Extensions;

namespace MeadowDesktopGtk.Display.DisplayElements
{
    public abstract class DisplayElement : IDisplayElement
    {
        private readonly bool _debugMode;
        private readonly bool _supportsColor;
        protected readonly Logger _logger;
        protected readonly MicroGraphics _graphics;
        private readonly int _boundingBoxStrokeThickness = 2;

        private Color BoundingBoxColor => _supportsColor
            ? Color.Default.Random()
            : Color.Black;

        protected IntVector DisplaySize => new IntVector(
            this._graphics.Width,
            this._graphics.Height
        );

        /**
         * @Summary position of the display element of unknown origin
        */
        public IntVector Position { get; }

        /**
         * @Summary the size of the bounding box in pixels
         */
        public abstract IntVector BoundingBoxSize { get; }

        /**
        * @Summary Top-Left origin position of the bounding box
        */
        public abstract IntVector BoundingBoxPosition { get; }

        public DisplayElement(
            MicroGraphics graphics,
            IntVector position,
            Logger logger,
            bool debugMode,
            bool supportsColor
        )
        {
            this.Position = position;
            this._graphics = graphics;
            this._debugMode = debugMode;
            this._logger = logger;
            this._supportsColor = supportsColor;
        }

        public DisplayElement(
            MicroGraphics graphics,
            IntVector position,
            Logger logger
        ) : this(graphics, position, logger, false, false)
        {            
        }

        public virtual void Draw()
        {
            if (this._debugMode)
            {
                this.DrawBoundingBox();
            }
        }

        public void DrawBoundingBox()
        {
            this.DrawBoundingBox(BoundingBoxPosition, BoundingBoxSize, BoundingBoxColor);
        }

        /**
         * @Summary Draws a rectangular bounding box
         * @Param position top-left origin position of the bounding box
         */
        private void DrawBoundingBox(IntVector position, IntVector size, Color color)
        {
            _logger.Info($"Drawing bounding box at {position} of size {size}");

            this.SetPen(color, this._boundingBoxStrokeThickness);
            _graphics.DrawRectangle(
                position.X,
                position.Y,
                size.X,
                size.Y,
                false
            );
            this.DrawCross(position, size);
            this.DrawOrientationIndicator(position);
        }

        /**
         * @Summary Draws orientation indicator
         * @Details If mixing up two coordinates (for instance via math)
         *          it is hard to know which direction certain elements
         *          are drawn. This method draws a an indicator to the 
         *          top-left origin.
         * @Param position top-left origin position of the cross to be drawn
         */
        private void DrawOrientationIndicator(
            IntVector position,
            int maxIndicatorLength = 30
        )
        {
            _logger.Info($"Drawing orientation indicator at {position} of length {maxIndicatorLength}");

            // limit to bounding box size for cases where the bounding box is smaller than the requested indicator
            var indicatorLengthX = Math.Min(this.BoundingBoxSize.X, maxIndicatorLength);
            var indicatorLengthY = Math.Min(this.BoundingBoxSize.Y, maxIndicatorLength);
            this.WithinDrawingSession(() =>
            {
                this.SetPenThickness(this._boundingBoxStrokeThickness * 3);
                // origin => right
                _graphics.DrawLine(
                    position.X,
                    position.Y,
                    position.X + indicatorLengthX,
                    position.Y);
                // origin => down
                _graphics.DrawLine(
                    position.X,
                    position.Y,
                    position.X,
                    position.Y + maxIndicatorLength);
            });
        }

        /**
         * @Summary Draws a cross of rectangular size
         * @Details +--+
         *          |\/|
         *          |/\|
         *          +--+
         * @Param position top-left origin position of the cross to be drawn
         */
        private void DrawCross(IntVector position, IntVector size)
        {
            _logger.Info($"Drawing bounding box cross at {position} of size {size}");

            // line top-left => bottom-right
            _graphics.DrawLine(
               position.X,
               position.Y,
               position.X + size.X,
               position.Y + size.Y
           );
            // line bottom-left => top-right
            _graphics.DrawLine(
                position.X,
                position.Y + size.Y,
                position.X + size.X,
                position.Y
            );
        }

        protected void SetPen(Color color, int thickness = 1)
        {
            this.SetPenColor(color);
            this.SetPenThickness(thickness);
        }

        protected void SetPenColor(Color color)
        {
            var previousColor = this._graphics.PenColor;
            var colorChanged = previousColor != color;
            if (!colorChanged)
            {
                return;
            }

            this._logger.Debug($"Adjusting font from {previousColor} => {color}");
            this._graphics.PenColor = color;
        }

        protected void SetFont(IFont font)
        {
            var previousFont = this._graphics.CurrentFont;
            var fontChanged = previousFont != font;
            if (!fontChanged)
            {
                return;
            }

            this._logger.Debug($"Adjusting font from {previousFont} => {font}");
            this._graphics.CurrentFont = font;
        }

        protected void SetPenThickness(int thickness)
        {
            var previousThickness = this._graphics.Stroke;
            var thicknessChanged = previousThickness != thickness;
            if (!thicknessChanged)
            {
                return;
            }
            this._logger.Debug($"Adjusting stroke thickness from {previousThickness} => {thickness}");
            this._graphics.Stroke = thickness;
        }

        protected void WithinDrawingSession(Action action)
        {
            this._graphics.SaveState();
            try
            {
                action();
            }
            finally
            {
                this._graphics.RestoreState();
            }
        }
    }
}