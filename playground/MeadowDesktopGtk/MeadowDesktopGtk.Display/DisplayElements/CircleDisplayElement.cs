using Meadow.Foundation;
using Meadow.Foundation.Graphics;
using Meadow.Logging;

namespace MeadowDesktopGtk.Display.DisplayElements
{

    public class CircleDisplayElement : DisplayElement
    {
        public int Radius { get; set; }
        public Color Color { get; set; }

        public override IntVector BoundingBoxSize => new IntVector(this.Diameter, this.Diameter);

        public override IntVector BoundingBoxPosition => new IntVector(
            this.Position.X - this.Radius,
            this.Position.Y - this.Radius
        );

        public int Diameter => 2 * this.Radius;

        public CircleDisplayElement(
            MicroGraphics graphics,
            IntVector positionCenter,
            int radius,
            Color color,
            Logger logger,
            bool debugMode = false,
            bool supportsColor = false
        ) : base(graphics, positionCenter, logger, debugMode, supportsColor)
        {
            this.Radius = radius;
            this.Color = color;
        }

        public override void Draw()
        {
            base.Draw();

            this._logger.Info("Drawing circle");
            this._graphics.PenColor = this.Color;
            this._graphics.DrawCircle(this.Position.X, this.Position.Y, this.Radius, true, true);
        }
    }
}