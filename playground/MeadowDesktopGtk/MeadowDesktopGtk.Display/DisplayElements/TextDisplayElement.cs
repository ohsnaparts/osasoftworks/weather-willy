using Meadow.Foundation;
using Meadow.Foundation.Graphics;
using Meadow.Logging;

namespace MeadowDesktopGtk.Display.DisplayElements
{
    public class TextDisplayElement : DisplayElement
    {
        private readonly string _text;
        private readonly Color _textColor;
        private readonly IFont _font;

        public TextDisplayElement(
            MicroGraphics graphics,
            IntVector position,
            Logger logger,
            string text,
            Color textColor,
            IFont font,
            bool debugMode = false,
            bool supportsColor = false
        ) : base(graphics, position, logger, debugMode, supportsColor)
        {
            this._text = text;
            this._textColor = textColor;
            this._font = font;
        }

        public override IntVector BoundingBoxSize
        {
            get
            {
                var textSize = this._graphics.MeasureText(this._text, this._font, ScaleFactor.X1);
                return new IntVector(textSize.Width, textSize.Height);
            }
        }

        // assuming the text to be centered both horizontally and vertically / center origin
        public override IntVector BoundingBoxPosition => new IntVector(
            this.Position.X - this.BoundingBoxSize.X / 2,
            this.Position.Y - this.BoundingBoxSize.Y / 2
        );

        public override void Draw()
        {
            base.Draw();
            this.WithinDrawingSession(() =>
            {
                this._graphics.PenColor = this._textColor;
                this._graphics.CurrentFont = this._font;
                this._graphics.DrawText(
                    this.Position.X,
                    this.Position.Y,
                    this._text,
                    ScaleFactor.X1,
                    HorizontalAlignment.Center,
                    VerticalAlignment.Center
                );
            });
        }
    }
}