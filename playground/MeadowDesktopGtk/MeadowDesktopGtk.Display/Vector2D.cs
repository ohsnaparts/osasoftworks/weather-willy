using System;

namespace MeadowDesktopGtk.Display;

public class Vector2D<T> where T: struct, IComparable<T>
{
    public Vector2D(T x, T y) : this((x ,y))
    {
    }

    public Vector2D((T X, T Y) vectorTuple) {
        this.VectorTuple = vectorTuple;
    }

    public (T X, T Y) VectorTuple { get; }
    public T X => this.VectorTuple.X;
    public T Y => this.VectorTuple.Y;

    public T Max(T v1, T v2) => v1.CompareTo(v2) > 0 ? v1 : v2;
    public T Min(T v1, T v2) => v1.CompareTo(v2) < 0 ? v1 : v2;

    public override string ToString()
    {
        return this.VectorTuple.ToString();
    }
}
