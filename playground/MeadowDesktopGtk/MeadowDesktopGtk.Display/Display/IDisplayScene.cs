﻿namespace MeadowDesktopGtk.Display.Display;

public interface IDisplayScene
{
    public void Render();
}
