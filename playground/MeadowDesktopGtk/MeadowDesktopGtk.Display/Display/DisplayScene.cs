using System;
using Meadow.Foundation;
using Meadow.Foundation.Graphics;
using Meadow.Logging;
using MeadowDesktopGtk.Common.Configuration;
using MeadowDesktopGtk.Display.DisplayElements;
using Microsoft.Extensions.Options;

namespace MeadowDesktopGtk.Display.Display;

public class DisplayScene : IDisplayScene
{
    private static readonly Color ColorPrimary = Color.Black;
    private static readonly Color ColorSecondary = Color.Red;

    private static readonly bool Filled = true;

    private readonly MicroGraphics _graphics;
    private readonly IOptions<SceneSettings> _sceneConfig;
    private readonly IOptions<DisplaySettings> _displaySettings;
    private readonly Logger _logger;

    private int Width => this._graphics.Width;
    private int Height => this._graphics.Height;
    private DisplaySettings DisplaySettings => this._displaySettings.Value;
    private SceneSettings SceneSettings => this._sceneConfig.Value;

    public DisplayScene(
        MicroGraphics graphics,
        IOptions<SceneSettings> sceneConfig,
        IOptions<DisplaySettings> displayConfig,
        Logger logger
    )
    {
        this._graphics = graphics;
        this._sceneConfig = sceneConfig;
        this._displaySettings = displayConfig;
        this._logger = logger;
    }

    public void Render()
    {
        _logger.Debug("Rendering scene");
        this.FillBackground(Color.White);
        this.DrawCoordinateGrid(IntVector.Zero, this._graphics.DisplaySize());
        this.DrawCircle();
        this.DrawImage(this.SceneSettings.BackgroundImagePath);

        this._graphics.ShowBuffered();
    }

    private void FillBackground(Color color)
    {
        _logger.Debug($"Filling background with color {color}");
        this._graphics.PenColor = color;
        this._graphics.DrawRectangle(
            0,
            0,
            this.Width,
            this.Height,
            Filled
        );
    }

    private void DrawCircle()
    {
        bool supportsColor = this.DisplaySettings.SupportsColor;
        var circle = new CircleDisplayElement(
                    this._graphics,
                    this._graphics.DisplayCenter() - (0, 50),
                    (int)(this.Width / 3),
                    supportsColor ? ColorSecondary : ColorPrimary,
                    this._logger,
                    this.SceneSettings.DebugMode,
                    supportsColor
                );
        
        circle.Draw();
        DrawTime(circle.Position, Color.White);
    }

    private void DrawTime(IntVector position, Color color) {
        var now = DateTime.Now;
        var time = $"{now.Hour}:{now.Minute}:{now.Second}";

        new TextDisplayElement(
            _graphics,
            position,
            _logger,
            time,
            color,
            new Font8x12(),
            this.SceneSettings.DebugMode,
            this.DisplaySettings.SupportsColor
        ).Draw();
    }

    private void DrawCoordinateGrid(IntVector start, IntVector size)
    {
        var debugMode = this.SceneSettings.DebugMode;
        var supportsColor = this.DisplaySettings.SupportsColor;

        this._logger.Info("DrawCoordinateGrid DebugMode " + (debugMode ? "enabled" : "disabled"));
        this._logger.Info("DrawCoordinateGrid Supports Color " + (supportsColor ? "enabled" : "disabled"));
        new CoordinateGridDisplayElement(
            this._graphics,
            start,
            start + size,
            ColorPrimary,
            new Font4x6(),
            this._logger,
            debugMode,
            supportsColor
        ).Draw();
    }

    private void DrawImage(string bitmapResourcePath)
    {
        this._logger.Info($"Drawing image resource {bitmapResourcePath}");
        var image = Image.LoadFromResource(bitmapResourcePath);
        new ImageDisplayElement(
            this._graphics,
            new IntVector(30, 135),
            image,
            this._logger,
            treatWhiteAsTransparent: false,
            this.SceneSettings.DebugMode,
            this.DisplaySettings.SupportsColor
        ).Draw();
    }    
}