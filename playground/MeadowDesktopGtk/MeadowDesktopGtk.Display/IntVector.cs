using System.Numerics;
using Meadow.Foundation.Graphics;
namespace MeadowDesktopGtk.Display;


public class FloatVector : Vector2D<float>
{
    public FloatVector(float x, float y) : base(x, y)
    {
    }
}

public class IntVector : Vector2D<int>
{
    public static readonly IntVector Zero = new IntVector(0, 0);
    
    public IntVector(int x, int y) : base(x, y)
    {
    }

    // Sums +
    public static IntVector operator +(IntVector augend, (int X, int Y) addend) => new IntVector(
        augend.X + addend.X,
        augend.Y + addend.Y
    );
    public static IntVector operator +(IntVector augend, int addend) => augend + (addend, addend);
    public static IntVector operator +(IntVector augend, IntVector addend) => augend + (addend.X, addend.Y);
    public static IntVector operator +(IntVector augend, Size addend) => augend + (addend.Width, addend.Height);

    // Subtractions -
    public static IntVector operator -(IntVector minuend, (int X, int Y) subtrahend) => new IntVector(
        minuend.X - subtrahend.X,
        minuend.Y - subtrahend.Y
    );
    public static IntVector operator -(IntVector minuend, int subtrahend) => minuend - (subtrahend, subtrahend);

    // Divisions -
    public static IntVector operator /(IntVector dividend, (int X, int Y) divisor) => new IntVector(
       dividend.X / divisor.X,
       dividend.Y / divisor.Y
   );
    public static IntVector operator /(IntVector dividend, int divisor) => dividend / (divisor, divisor);
    public static IntVector operator /(IntVector dividend, IntVector divisor) => dividend / (divisor.X, divisor.Y);

    // Multiplications *
    public static IntVector operator *(IntVector multiplier, (int X, int Y) multiplicand) => new IntVector(
       multiplier.X * multiplicand.X,
       multiplier.Y * multiplicand.Y
   );
    public static IntVector operator *(IntVector multiplier, int multiplicand) => multiplier * (multiplicand, multiplicand);
    public static FloatVector operator *(IntVector multiplier, (float X, float Y) multiplicand) => new FloatVector(
        multiplier.X * multiplicand.X,
        multiplier.Y * multiplicand.Y
    );
}
