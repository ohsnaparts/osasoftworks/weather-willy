using System.Runtime.CompilerServices;
using Meadow.Foundation;
using Meadow.Foundation.Graphics;
using MeadowDesktopGtk.Display;


public static class DisplayExtensions
{
    public static IntVector DisplayCenter(this MicroGraphics graphics) => graphics.DisplaySize() / 2;

    public static IntVector DisplaySize (this MicroGraphics graphics) => new IntVector(
        graphics.Width,
        graphics.Height
    );

    public static void DrawImage(
        this MicroGraphics graphics,
        IntVector positionTopLeftOrigin,
        Image image
    ) => graphics.DrawImage(
        positionTopLeftOrigin.X,
        positionTopLeftOrigin.Y,
        image
    );

    public static void DrawRectangle(
        this MicroGraphics graphcis,
        IntVector positionTopLeftOrigin,
        IntVector size,
        Color color,
        bool filled = false
    ) => graphcis.DrawRectangle(
        positionTopLeftOrigin.X,
        positionTopLeftOrigin.Y,
        size.X,
        size.Y,
        color,
        filled
    );
}