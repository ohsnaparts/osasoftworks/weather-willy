using System.Drawing;

namespace MeadowDesktopGtk.Display.Extensions
{
    public static class ColorToMeadowColorExtensions
    {
        public static Meadow.Foundation.Color ToMeadowColor(this Color color) => Meadow.Foundation.Color.FromRgba(
            color.R,
            color.G,
            color.B,
            color.A
        );
    }
}