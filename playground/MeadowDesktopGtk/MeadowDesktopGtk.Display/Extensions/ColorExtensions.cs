namespace MeadowDesktopGtk.Display.Extensions;

using System;
using Meadow.Foundation;

public static class ColorExtensions {

    public static Color ToRgbColor(this byte[] rgbValues) {
        if(rgbValues.Length != 3) {
            throw new ArgumentException(
                $"The byte array provides only {rgbValues.Length} out of 3 necessary rgb channel values!",
                nameof(rgbValues)
            );
        }
        return Color.FromRgb(
            rgbValues[0],
            rgbValues[1],
            rgbValues[2]
        );
    }

    public static Color Random(this Color color)
    {
        byte[] rgbValues = new byte[3];
        new Random().NextBytes(rgbValues);
        return rgbValues.ToRgbColor();

    }

    public static byte Average(this Color color) => (byte)((color.R + color.G + color.B) / 3);
}