ERROR_CODE_POWERSHELL_MISSING=100
PWSH=pwsh

if ! $PWSH -Version; then
    echo
    echo "This script requires powershell ($PWSH) to be installed and available your PATH environment"
    echo "To install it on linux, please refer to the following resource:"
    echo
    echo "https://learn.microsoft.com/en-us/powershell/scripting/install/installing-powershell-on-linux"
    echo
    exit $ERROR_CODE_POWERSHELL_MISSING
fi

$PWSH -Command ./run-embedded.ps1
