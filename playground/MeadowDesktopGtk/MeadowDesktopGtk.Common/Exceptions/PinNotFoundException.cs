using System;

namespace MeadowDesktopGtk.Common.Exceptions;

public class PinNotFoundException : Exception {
    public PinNotFoundException(string pinName, Exception inner) : base(
        $"A GPIO pin with name {pinName} could not be found.",
        inner
    )
    {
    }
}