using Meadow;

namespace MeadowDesktopGtk.Common.Configuration;

public class LoggingSettings : ConfigurableObject
{
    public bool ShowTicks => GetConfiguredBool(nameof(ShowTicks), false);
}