using Meadow;

namespace MeadowDesktopGtk.Common.Configuration;

public class DisplaySettings : ConfigurableObject
{
    public int Width => GetConfiguredInt(nameof(Width), 0);
    public int Height => GetConfiguredInt(nameof(Height), 0);
    public bool SupportsColor => GetConfiguredBool(nameof(SupportsColor), false);
    public double FullRefreshIntervalSeconds => GetConfiguredFloat(nameof(FullRefreshIntervalSeconds), 0);
    public double MinSafeRenderIntervalSeconds => GetConfiguredFloat(nameof(MinSafeRenderIntervalSeconds), 0);
    public int SpiFrequencyHz => GetConfiguredInt(nameof(SpiFrequencyHz), 0);
    public string PinSpiChipSelect => GetConfiguredString(nameof(PinSpiChipSelect), string.Empty);
    public string PinSpiClock => GetConfiguredString(nameof(PinSpiClock), string.Empty);
    public string PinSpiMiso => GetConfiguredString(nameof(PinSpiMiso), string.Empty);
    public string PinSpiMosi => GetConfiguredString(nameof(PinSpiMosi), string.Empty);
    public string PinDataCommand => GetConfiguredString(nameof(PinDataCommand), string.Empty);
    public string PinReset => GetConfiguredString(nameof(PinReset), string.Empty);
    public string PinBusy => GetConfiguredString(nameof(PinBusy), string.Empty);
    public string PinDisplaySramChipSelect => GetConfiguredString(nameof(PinDisplaySramChipSelect), string.Empty);

    public TimeSpan MinSafeRenderInterval => TimeSpan.FromSeconds(this.MinSafeRenderIntervalSeconds);
}
