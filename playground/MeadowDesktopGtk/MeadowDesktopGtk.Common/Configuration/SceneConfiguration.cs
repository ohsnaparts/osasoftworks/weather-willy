using Meadow;

namespace MeadowDesktopGtk.Common.Configuration;


public class SceneSettings : ConfigurableObject
{
    /**
     * Resource path
     */
    public string BackgroundImagePath => GetConfiguredString(nameof(BackgroundImagePath), string.Empty);
    public bool DebugMode => GetConfiguredBool(nameof(DebugMode), false);
}
