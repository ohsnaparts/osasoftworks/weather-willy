using Meadow.Logging;
using Microsoft.Extensions.DependencyInjection;

namespace MeadowDesktopGtk.Common.IoC;

public static class ServiceProviderExtensions
{
    public static void Log(this IServiceProvider serviceProvider, string message)
    {
        var logger = serviceProvider.GetService<Logger>();
        if(logger == null){
            return;
        }
        logger.Info(message);
    }
}