using MeadowDesktopGtk.Common.Configuration;
using Meadow;
using Meadow.Logging;
using Microsoft.Extensions.Options;
using MeadowDesktopGtk.Common.IoC.MinIoC;
using MeadowDesktopGtk.Common.IoC;
using Microsoft.Extensions.DependencyInjection;

public static class MeadowDeviceIocExtensions
{
    public static Container SetupCommonIoC<TDevice>(
        this App<TDevice> app
    ) where TDevice : class, IMeadowDevice => new Container()
        // always regsiter logging first
        .RegisterLogging()
        .RegisterDevice()
        .RegisterSettings<DisplaySettings>()
        .RegisterSettings<SceneSettings>();

    public static Container RegisterDevice(this Container container)
    {
        container.Log($"Registering device");
        container.Register<IMeadowDevice>(() => Resolver.Device).AsSingleton();
        return container;
    }

    private static Container RegisterLogging(this Container container)
    {
        container
            .RegisterSettings<LoggingSettings>()
            .Register<Logger>(services =>
            {
                var logger = Resolver.Log;                
                var config = services.GetRequiredService<IOptions<LoggingSettings>>().Value;
                logger.ShowTicks = config.ShowTicks;

                return logger;
            }).AsSingleton();

        return container;
    }
}