using MeadowDesktopGtk.Common.IoC.MinIoC;
using Microsoft.Extensions.Options;

namespace MeadowDesktopGtk.Common.IoC;

// ReSharper disable once UnusedType.Global
public static class ContainerExtensions
{
    public static Container.IRegisteredType Register<TInterface>(
        this Container container,
        Func<object> factory
    ) => container.Register<TInterface>((_) => factory());

    public static Container.IRegisteredType Register<TInterface>(
        this Container container,
        Func<IServiceProvider, object> factory
    )
    {
        try
        {
            container.Log($"Registering factory for {typeof(TInterface)}");
            return container.Register(typeof(TInterface), () => factory(container));
        }
        catch (Exception ex)
        {
            throw new Exception(
                "Unhandled exception caught trying to register" +
                $" type {typeof(TInterface).FullName}!", ex
            );
        }
    }

    public static Container.IRegisteredType Register<TInterface>(
        this Container container,
        object value
    ) => container.Register<TInterface>(() => value);

    public static Container.IRegisteredType Register<TInterface, TImplementation>(this Container container)
    {
        try
        {
            container.Log($"Registering {typeof(TImplementation).FullName} as {typeof(TInterface).FullName}.");
            return container.Register(typeof(TInterface), typeof(TImplementation));
        }
        catch (Exception ex)
        {
            throw new Exception(
                "Unhandled exception caught trying to register" +
                $" type {typeof(TImplementation).FullName} as {typeof(TInterface).FullName}!", ex
            );
        }
    }

    /**
     * Registers configuration for TOption as injectable IOptions type.
     * <param name="container">The ioc container to register the options into</param>
     * This string consists of section keys separated by colons.</param>
     * @see https://beta-developer.wildernesslabs.co/Meadow/Meadow.OS/Configuration/Application_Settings_Configuration/
     */
    public static Container RegisterSettings<TOption>(this Container container)
        where TOption : class, new()
    {
        container.Log($"Registering settings {typeof(TOption).Name}");
        container.Register<IOptions<TOption>>(services =>
            Options.Create(new TOption())
        ).AsSingleton();
        return container;
    }
}