using System;
using Meadow;
using Meadow.Hardware;
using MeadowDesktopGtk.Common.Exceptions;

namespace MeadowDesktopGtk.Common.Extensions;

public static class MeadowDeviceExtensions
{
    public static IPin GetPinSafe(
        this IMeadowDevice device,
        string pinName
    )
    {
        try
        {
            return device.GetPin(pinName);
        }
        catch (Exception ex)
        {
            throw new PinNotFoundException(pinName, ex);
        }
    }

    public static IDigitalOutputPort CreateDigitalOutputPort(this IMeadowDevice device, string pinName) => device
        .GetPinSafe(pinName)
        .CreateDigitalOutputPort();

    public static IDigitalInputPort CreateDigitalInputPort(this IMeadowDevice device, string pinName) => device
        .GetPinSafe(pinName)
        .CreateDigitalInputPort();
}
