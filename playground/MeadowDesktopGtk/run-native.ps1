$BuildConfiguration = 'Debug'.ToLower();
$ProjectDirPath = Join-Path $PSScriptRoot 'MeadowDesktopGtk.Native/'

dotnet run --configuration $BuildConfiguration --project $ProjectDirPath
