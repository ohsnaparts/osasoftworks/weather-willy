using Meadow.Hardware;

internal class DigitalInputPortDecoratorBase : IDigitalInputPortDecoratorBase
{
    private readonly IDigitalInputPort port;

    public DigitalInputPortDecoratorBase(IDigitalInputPort port)
    {
        this.port = port;
    }

    public bool State { get => this.port.State; }

    public IDigitalChannelInfo Channel => this.port.Channel;

    public IPin Pin => this.port.Pin;

    public ResistorMode Resistor
    {
        get => this.port.Resistor;
        set => this.port.Resistor = value;
    }

    public void Dispose()
    {
        this.port.Dispose();
    }
}