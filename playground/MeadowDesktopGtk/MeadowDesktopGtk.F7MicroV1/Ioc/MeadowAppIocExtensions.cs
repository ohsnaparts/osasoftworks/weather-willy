using System;
using Meadow;
using Meadow.Foundation.Displays;
using Meadow.Foundation.Graphics;
using Meadow.Hardware;
using Meadow.Units;
using MeadowDesktopGtk.Common.Configuration;
using MeadowDesktopGtk.Common.IoC;
using MeadowDesktopGtk.Common.IoC.MinIoC;
using MeadowDesktopGtk.Display.Display;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Options;
using MeadowDesktopGtk.Common.Extensions;
using Meadow.Logging;

namespace MeadowDesktopGtk.F7MicroV1.Ioc;

internal static class MeadowAppIocExtensions
{
    // Be careful when reordering, a registration step might depend on parent registrations
    public static IServiceProvider SetupIoc(this MeadowApp app, IMeadowDevice device) => app
        .SetupCommonIoC()
        .RegisterSpiBus(device)
        .RegisterGraphics();

    public static Container RegisterSpiBus(
        this Container container,
        IMeadowDevice device
    )
    {
        container.Log("Registering SPI...");
        container.Register<ISpiBus>(serviceProvider =>
        {
            var config = serviceProvider.GetRequiredService<IOptions<DisplaySettings>>().Value;

            container.Log($"""
                Creating SPI Bus with configuration:
                Clock: {config.PinSpiClock}
                MOSI: {config.PinSpiMosi}
                MISO: {config.PinSpiMiso}
                Frequency: {config.SpiFrequencyHz} hz
            """);

            try
            {
                return device.CreateSpiBus(
                    device.GetPinSafe(config.PinSpiClock),
                    device.GetPinSafe(config.PinSpiMosi),
                    device.GetPinSafe(config.PinSpiMiso),
                    new Frequency(config.SpiFrequencyHz, Frequency.UnitType.Hertz)
                );
            }
            catch (Exception ex)
            {
                throw new Exception("Unable to creat eSPI bus", ex);
            }

        }).AsSingleton();
        return container;
    }

    public static Container RegisterGraphics(this Container container)
    {
        container.Log("Initializing display...");
        container.Register<IGraphicsDisplay>(serviceProvider =>
        {
            var config = serviceProvider.GetRequiredService<IOptions<DisplaySettings>>().Value;
            var device = serviceProvider.GetRequiredService<IMeadowDevice>();

            container.Log($"""
                Creating SSD1680 Graphics Display with configuration:
                ChipSelect: {config.PinSpiChipSelect}
                DataCommand: {config.PinDataCommand}
                Reset: {config.PinReset}
                Busy: {config.PinBusy}
                Width: {config.Width}
                Height: {config.Height}
            """);
            return new Ssd1680(
                   spiBus: serviceProvider.GetRequiredService<ISpiBus>(),
                   chipSelectPin: device.GetPinSafe(config.PinSpiChipSelect),
                   dcPin: device.GetPinSafe(config.PinDataCommand),
                   resetPin: device.GetPinSafe(config.PinReset),
                   busyPin: device.GetPinSafe(config.PinBusy),
                   width: config.Width,
                   height: config.Height
            );
        }).AsSingleton();

        container.Register<MicroGraphics>(services => new MicroGraphics(
            services.GetRequiredService<IGraphicsDisplay>()
        ) {
            CurrentFont = new Font8x8()
        }).AsSingleton();

        container.Log("Registering display scene...");
        container.Register<IDisplayScene>(services => new DisplayScene(
            services.GetRequiredService<MicroGraphics>(),
            services.GetRequiredService<IOptions<SceneSettings>>(),
            services.GetRequiredService<IOptions<DisplaySettings>>(),
            services.GetRequiredService<Logger>()
        ));

        return container;
    }
}