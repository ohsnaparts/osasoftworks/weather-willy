﻿using System;
using System.Threading;
using System.Threading.Tasks;
using Meadow;
using Meadow.Devices;
using Meadow.Foundation;
using Meadow.Foundation.Graphics;
using Meadow.Foundation.Leds;
using Meadow.Hardware;
using Meadow.Peripherals.Leds;
using MeadowDesktopGtk.Common.Configuration;
using MeadowDesktopGtk.Common.Extensions;
using MeadowDesktopGtk.Display.Display;
using MeadowDesktopGtk.Display.Extensions;
using MeadowDesktopGtk.F7MicroV1.Ioc;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Options;

namespace MeadowDesktopGtk.F7MicroV1
{

    // Change F7FeatherV2 to F7FeatherV1 for V1.x boards
    public class MeadowApp : App<F7FeatherV1>
    {
        private readonly Meadow.Logging.Logger _logger = Resolver.Log;
        private CancellationToken _cancellationTokenSource = new CancellationTokenSource().Token;
        private RgbPwmLed _onboardLed;
        private IDisplayScene _displayScene;
        private MicroGraphics _graphics;

        // For some reason colors are inverted
        private static Color ColorWhite = Color.White;
        private static Color ColorBlack = Color.Black;
        private static Color ColorPrimary = ColorBlack;
        private static Color ColorSecondary = ColorBlack;
        private IOptions<DisplaySettings> _displayConfig;

        private DisplaySettings DisplayConfig => this._displayConfig.Value;

        public override Task Initialize()
        {
            this._logger.Info("Initialize...");
            IServiceProvider services = this.SetupIoc(Device);

            this._logger.Debug("Initializing onboard Leds");
            this._onboardLed = new RgbPwmLed(
                redPwmPin: Device.Pins.OnboardLedRed,
                greenPwmPin: Device.Pins.OnboardLedGreen,
                bluePwmPin: Device.Pins.OnboardLedBlue,
                CommonType.CommonAnode);

            this._logger.Debug("Initializing Micrographics");
            this._graphics = services.GetRequiredService<MicroGraphics>();
            this._displayConfig = services.GetRequiredService<IOptions<DisplaySettings>>();
            this._displayScene = services.GetRequiredService<IDisplayScene>();
            this.useEpaperSram(false);

            return base.Initialize();
        }

        public override async Task Run()
        {
            Resolver.Log.Info("Drawing on display");
            this.TurnOnLed();

            await InvokeAsync(this.RunSafetyScreenRefreshAsync);

            while (!this._cancellationTokenSource.IsCancellationRequested)
            {
                Invoke(() =>
                {
                    this._displayScene.Render();
                });

                Invoke(() =>
                {
                    this._logger.Info("Showing Graphics on Display...");
                    this._graphics.Show();
                });

                this._logger.Info($"Sleeping off the minimal safe e-paper re-render interval...");
                EnableLowPowerMode(_displayConfig.Value.MinSafeRenderIntervalSeconds);
            }

            Invoke(() =>
            {
                this._logger.Info("Run complete");
                this.TurnOffLed();
            });

            await base.Run();
        }

        private void EnableLowPowerMode(double seconds)
        {
            var time = TimeSpan.FromSeconds(seconds);
            this._logger.Info($"Switching into low-power mode for {time.TotalSeconds} seconds");
            this._logger.Info($"Be right back <3");
            Device.PlatformOS.Sleep(time);
        }

        private void useEpaperSram(bool enable)
        {
            this._logger.Debug((enable ? "Enabling" : "Disabling") + " EPaper SRAM");
            IDigitalOutputPort slaveSelectPort = CreateDisplaySlaveSelectPort();
            slaveSelectPort.State = !enable;
        }

        private IDigitalOutputPort CreateDisplaySlaveSelectPort() => Device.CreateDigitalOutputPort(
            this.DisplayConfig.PinDisplaySramChipSelect
        );

        private Task RunSafetyScreenRefreshAsync(CancellationToken cancellationToken = default)
        {
            this._logger.Debug("Running a full screen refresh to minimize memory effects...");
            this._graphics.Clear(true);

            var interval = TimeSpan.FromSeconds(this.DisplayConfig.FullRefreshIntervalSeconds);
            this._logger.Info($"Waiting {interval.TotalSeconds} seconds...");
            return Task.Delay(interval, cancellationToken);
        }

        private async Task InvokeAsync(Func<CancellationToken, Task> action, CancellationToken cancellationToken = default)
        {
            SetLed(Color.Default.Random());
            await action(cancellationToken);
        }

        private void Invoke(Action action)
        {
            SetLed(Color.Default.Random());
            action();
        }

        private void SetLed(Color color, float brightness = 1.0f)
        {
            _onboardLed.SetColor(color, brightness);
        }

        private void SetLed(float brightness = 1.0f) => this.SetLed(
            this._onboardLed.Color, brightness
        );

        private void SetLed(bool on) => this.SetLed(on ? 1.0f : 0.0f);

        private void TurnOffLed() => this.SetLed(false);
        private void TurnOnLed() => this.SetLed(true);
    }
}