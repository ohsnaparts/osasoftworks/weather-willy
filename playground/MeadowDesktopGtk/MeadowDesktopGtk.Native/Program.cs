﻿using Meadow;
using Meadow.Foundation.Displays;
using Meadow.Logging;
using Meadow.Pinouts;
using MeadowDesktopGtk.Common.Configuration;
using MeadowDesktopGtk.Display.Display;
using MeadowDesktopGtk.Native.Ioc;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Options;

namespace MeadowDesktopGtk.Native;

// ReSharper disable once UnusedType.Global
public class MeadowApp : App<Linux<WSL2>>
{
    private CancellationToken _cancellationTokenSource = new CancellationTokenSource().Token;
    private GtkDisplay _display = null!;
    private IDisplayScene _displayScene = null!;
    private IOptions<DisplaySettings> _displaySettings;
    private Logger _logger = null!;

    static async Task Main(string[] args)
    {
        await MeadowOS.Start(args);
    }

    public override Task Initialize()
    {
        var services = this.SetupIoc();
        this._display = services.GetRequiredService<GtkDisplay>();
        this._displayScene = services.GetRequiredService<IDisplayScene>();
        this._logger = services.GetRequiredService<Logger>();
        this._displaySettings = services.GetRequiredService<IOptions<DisplaySettings>>();

        return base.Initialize();
    }

    public override async Task Run()
    {
        this._logger.Info("Run...");
        this._logger.Info("Hello, Meadow.Linux GTK Display!");

        var settings = Resolver.App.Settings;
        this._logger.Info($"Found {settings.Count} settings: {string.Join(',', settings.Keys)}");

        new Thread(async () => 
        {
            while (!this._cancellationTokenSource.IsCancellationRequested)
            {
                this._displayScene.Render();
                await Task.Delay(this._displaySettings.Value.MinSafeRenderInterval);
            }
        }).Start();


        this._display.Run();
        await base.Run();
    }
}