using Meadow;
using Meadow.Foundation.Displays;
using Meadow.Foundation.Graphics;
using Meadow.Logging;
using MeadowDesktopGtk.Common.Configuration;
using MeadowDesktopGtk.Common.IoC;
using MeadowDesktopGtk.Common.IoC.MinIoC;
using MeadowDesktopGtk.Display.Display;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Options;

namespace MeadowDesktopGtk.Native.Ioc;

internal static class MeadowAppIocExtensions
{
    // Be careful when reordering, a registration step might depend on parent registrations
    public static IServiceProvider SetupIoc(this MeadowApp app) => app
        .SetupCommonIoC()
        .RegisterLogger()
        .RegisterGraphics();

    private static Container RegisterLogger(this Container container)
    {
        container.Register<Logger>(services =>
        {
            var config = ServiceProviderServiceExtensions.GetRequiredService<IOptions<LoggingSettings>>(services).Value;

            var logger = Resolver.Log;
            logger.ShowTicks = config.ShowTicks;

            return logger;
        }).AsSingleton();
        container.Log("Registered logger!");

        return container;
    }

    private static Container RegisterGraphics(this Container container)
    {
        container.Log("Registering graphics...");
        container.Register<GtkDisplay>(serviceProvider =>
        {
            var config = serviceProvider.GetRequiredService<IOptions<DisplaySettings>>().Value;
            return new GtkDisplay(config.Width, config.Height, ColorMode.Format16bppRgb565);
        }).AsSingleton();
        container
            .Register<IGraphicsDisplay>(serviceProvider => serviceProvider.GetRequiredService<GtkDisplay>())
            .AsSingleton();
        container.Register<MicroGraphics>(serviceProvider =>
        {
            var config = serviceProvider.GetRequiredService<IOptions<DisplaySettings>>();
            return new MicroGraphics(serviceProvider.GetRequiredService<IGraphicsDisplay>())
            {
                IgnoreOutOfBoundsPixels = true

            };
        }).AsSingleton();

        container.Log("Registering display scene...");
        container.Register<IDisplayScene, DisplayScene>().AsSingleton();

        return container;
    }
}