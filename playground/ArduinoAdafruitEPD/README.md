# Arduino Adafruit EPD

This sample validates the physical display using the arduino libraries
already provided by adafruit.

![](./screenshots//Screenshot%20from%202023-12-08%2012-00-38.jpg)

## Requirements

1. Install Arduino IDE or VSCode with Arduino Extension
1. Install `Adafruit EPD` library
1. Connect an SSD1680 compatible adafruit e-paper display
1. Flash sample to device


## Wiring

```txt
     ┌────────────────────────────────────────────────────┐
     │       ARADRUIT 2.13IN EPAPER DISPLAY SSD1680       │
     │                                                    │
     │                                    SDCS            │
     │                                                    │
     │ VIN 3V3 GND SCK MISO MOSI ECS DC SRCS RST BUSY ENA │
     │  │       │   │   │     │   │  │   │    │   │       │
     └──┼───────┼───┼───┼─────┼───┼──┼───┼────┼───┼───────┘
        │       │   │   │     │   │  │   │    │   │
        └────┐  │   │   │     │   │  │   │    │   │
             │  │   │   │     │   │  │   │    │   │
┌────────────┼──┼───┼───┼─────┼───┼──┼───┼────┼───┼────────────────┐
│            │  │   │   │     │   │  │   │    │   │                │
│            │  │   │   │     │   │  │   │    │   │        ┌────┐  │
│            │  │   │   │     │   │  │   │    │   │        │    │  │
│            │  │   │   │     │   │  │   │    │   │        │SDA │  │
│   ┌──────┐ │  │   │   │     │   │  │   │    │   │        │    │  │
│   │ IDEF │ │  │   │   │     │   │  │   │    │   │        │AREF│  │
│   │      │ │  │   │   │     │   │  │   │    │   │        │    │  │
│   │ RESET│ │  │   │   │     │   │  │   │    │   │        │GND │  │
│  P│      │ │  │   │   │     │   │  │   │    │   │        ├────┤  │
│  O│ 3V3 ─┼─┘  │   │   │     │   │  │   │    │   │        │ 13 │  │
│  W│      │    │   │   │     │   │  │   │    │   │        │    │  │
│  E│ 5V   │    │   │   │     │   │  │   │    │   │        │ 12 │  │
│  R│      │    │   │   │     │   │  │   │    │   │        │    │D │
│   │ GND ─┼────┘   │   │     │   │  │   │    │   └────────┼─11 │I │
│   │      │        │   │     │   │  │   │    │            │    │G │
│   │ VIN  │        │   │     │   │  │   │    └────────────┼─10 │I │
│   └──────┘        │   │     │   │  │   │                 │    │T │
│                   │   │     │   │  │   └─────────────────┼─09 │A │
│  A┌──────┐        │   │     │   │  │                     │    │L │
│  N│ A0   │        │   │     │   │  └─────────────────────┼─08 │  │
│  A│      │        │   │     │   │                        │    │P │
│  L│ ..   │ ┌──────┴───┴───┐ │   └────────────────────────┼─07 │W │
│  O│      │ │ RST CLK MISO │ │                            │    │M │
│  G│ A5   │ │ GND MOSI 5V  │ │                            │ .. │  │
│   └──────┘ └──────┬───────┘ │                            │    │  │
│                   │         │                            │ 00 │  │
│                   └─────────┘                            └────┘  │
│                                                                  │
└──────────────────────────────────────────────────────────────────┘
                         ARDUINO LEONARDO ETH
```

![](./screenshots/arduino-leonardo-pinout-1536x1086.jpg)