#include "Adafruit_EPD.h"

#define DISPLAY_WIDTH 250
#define DISPLAY_HEIGHT 122
// epaper displays should not be refreshed in intervals smaller than 3 minutes
// or else they may get physical defects over time
#define MIN_REFRESH_DELAY_SECONDS (3 * 60)
#define SERIAL_BAUD_RATE 115200
#define EPD_CS     7
#define EPD_DC     8
#define SRAM_CS    9
#define EPD_RESET 10  // -1 to share microcontroller Reset
#define EPD_BUSY  11  // -1 to wait a fixed delay
#define EPD_SPI &SPI

#define TRI_COLOR_EPD 0
#define COLOR_BLACK EPD_BLACK
#define COLOR_WHITE EPD_WHITE
#define COLOR_PRIMARY COLOR_BLACK
#if TRI_COLOR_EPD == 1
  #define COLOR_SECONDARY EPD_RED
#else
  // fall back to mono
  #define COLOR_SECONDARY COLOR_PRIMARY
#endif

// display for the 2.13" EPD with SSD1680 driver
Adafruit_SSD1680 display(
  DISPLAY_WIDTH,
  DISPLAY_HEIGHT,
  EPD_DC,
  EPD_RESET,
  EPD_CS,
  SRAM_CS,
  EPD_BUSY,
  EPD_SPI);

void setup() {
  serial_init();
  Serial.println("Adafruit EPD test");
  
  display.begin();
  display_use_sram(false);

  Serial.println("Full refreshing once to prevent memory effects of previous samples");
  display_clear();
  
  display_draw_grid();
  display_draw_text(COLOR_PRIMARY);
  display_draw_circle();
  display_show();

  wait_seconds(MIN_REFRESH_DELAY_SECONDS);
}

void loop() {
  // don't do anything!
}

void await_serial() {
  while (!Serial) {
    delay(10); 
  }
}

void serial_init() {
  Serial.begin(SERIAL_BAUD_RATE);
  await_serial();
  Serial.println("Serial initialized...");
}

void wait_seconds(uint32_t seconds) {
  Serial.print("Waiting ");
  Serial.print(seconds);
  Serial.println(" seconds");
  if(seconds < 1) {
    return;
  }

  for(uint32_t s = 0; s < seconds; s++) {
    Serial.print(seconds - s);
    Serial.print("/");
    Serial.print(seconds);
    Serial.println("s");
    delay(1000);
  }
}

void display_clear() {
  Serial.println("Clearing display...");
  display.clearBuffer();
  display_show();
}

void display_draw_grid() {
  Serial.println("drawing horizontal lines");
  const int16_t spacing = 15;
  for (int16_t x = 0; x < display.width(); x += spacing) {
    display.drawLine(0, 0, x, display.height(), COLOR_PRIMARY);
  }

  Serial.println("drawing vertical line");
  for (int16_t y = spacing; y < display.height(); y += spacing) {
    display.drawLine(0, 0, display.width(), y, COLOR_PRIMARY);
  }
}

void display_draw_circle() {
  Serial.println("drawing circle");
  const int16_t center_x = display.width() / 2;
  const int16_t center_y = display.height() / 2;
  const int16_t circle_radius = center_y - center_y * 0.15;
  const int16_t thickness = circle_radius / 5;
  const int16_t margin = thickness;
  
  // margin
  // inner
  display.fillCircle(
    center_x,
    center_y,
    circle_radius + margin,
    COLOR_WHITE
  );

  // outer
  display.fillCircle(
    center_x,
    center_y,
    circle_radius,
    COLOR_SECONDARY);
  
  // inner
  display.fillCircle(
    center_x,
    center_y,
    circle_radius - thickness,
    COLOR_WHITE
  );
}

void display_draw_text(uint16_t color) {
  Serial.println("drawing text");
  display.setCursor(0, 0);
  display.setTextColor(color);
  display.setTextWrap(true);
  display.setTextColor(color);
  display.print(
    "Gummies toffee dragée gummies candy. Brownie cotton candy"
    "dragée sesame snaps chocolate cake halvah tart oat cake."
    "Lemon drops brownie pudding topping gingerbread chupa chups"
    "tiramisu gummi bears apple pie. Sweet sugar plum chupa chups"
    "tootsie roll halvah. Halvah oat cake lollipop gummi bears "
    "halvah. Cotton candy sugar plum topping halvah chocolate cake."
    "Jelly-o marshmallow cotton candy gummies biscuit soufflé jelly"
    "chups candy canes lemon drops chupa chups. Shortbread chocolate"
    "sugar plum jelly beans powder jelly beans. Sweet pastry muffin"
    "jelly beans chocolate cake caramels donut croissant sweet roll.");
}

void display_show() {
  Serial.println("display...");
  display.display();
}

void display_use_sram(bool enabled) {
  digitalWrite(SRAM_CS, enabled ? LOW : HIGH);
}
