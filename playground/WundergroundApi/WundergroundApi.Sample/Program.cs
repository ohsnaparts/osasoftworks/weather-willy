﻿using Microsoft.Extensions.Configuration;
using WundergroundApi.Api;
using WundergroundApi.Sample.Configuration;

Console.WriteLine("Hello, World!");


var configuration = new ConfigurationBuilder()
    .AddJsonFile($"appsettings.json")
    .Build();

var wundergroundConfig = configuration.GetSection(nameof(WundergroundConfiguration)).Get<WundergroundConfiguration>();
if (wundergroundConfig == null)
{
    throw new ArgumentNullException(nameof(wundergroundConfig));
}

var client = new WundergroundApi.Api.WundergroundApi(
    new HttpClient(),
    wundergroundConfig.ApiKey,
    wundergroundConfig.WeatherStationId
);

var observationResult = await client.GetPersonalWeatherStationCurrentObservationAsync();
PrintObservationReponse(observationResult);

Console.WriteLine($"Awaiting new values ~{wundergroundConfig.UpdateInterval.TotalSeconds} seconds");
await Task.Delay(wundergroundConfig.UpdateInterval);


void PrintObservationReponse(ObservationResponseDto response)
{
    foreach (var observation in response.Observations)
    {
        Console.WriteLine(observation.ToString());
    }
}