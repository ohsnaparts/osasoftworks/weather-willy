namespace WundergroundApi.Sample.Configuration
{
    public class WundergroundConfiguration
    {
        public double UpdateIntervalSeconds { get; set; } = 0.0;
        public string ApiKey { get; set; } = string.Empty;
        public string WeatherStationId { get; set; } = string.Empty;

        public TimeSpan UpdateInterval => TimeSpan.FromSeconds(this.UpdateIntervalSeconds);
    }
}