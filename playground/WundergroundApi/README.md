# Wunderground API

A minimalistic wunderground API

## Requirements

* .NET
* Wunderground API Key

## Run

```
dotnet test
# configure your api key first in app settings
dotnet run --project WundergroundApi.Sample
```