namespace WundergroundApi.Tests.Unit;

internal static class TestData
{
    public static Uri DummyUri = new Uri("http://blabla");

    public static readonly string GetPwsCurrentObservation = """
                                                             {
                                                              "observations": [
                                                                  {
                                                                  "stationID":"ISPITA18",
                                                                  "obsTimeUtc":"2023-11-18T14:08:33Z",
                                                                  "obsTimeLocal":"2023-11-18 15:08:33",
                                                                  "neighborhood":"Spital am Semmering",
                                                                  "softwareType":"EasyWeatherPro_V5.0.6",
                                                                  "country":"AT",
                                                                  "solarRadiation":37.1,
                                                                  "lon":15.789612,
                                                                  "realtimeFrequency":null,
                                                                  "epoch":1700316513,
                                                                  "lat":47.620013,
                                                                  "uv":0.0,
                                                                  "winddir":152,
                                                                  "humidity":55,
                                                                  "qcStatus":1,
                                                                  "imperial": {
                                                                      "temp":38,
                                                                      "heatIndex":38,
                                                                      "dewpt":23,
                                                                      "windChill":38,
                                                                      "windSpeed":0,
                                                                      "windGust":1,
                                                                      "pressure":29.95,
                                                                      "precipRate":0.00,
                                                                      "precipTotal":0.00,
                                                                      "elev":823
                                                                 }
                                                               }
                                                             ]
                                                             }
                                                             """;

    public static string DummyString => Guid.NewGuid().ToString();
}