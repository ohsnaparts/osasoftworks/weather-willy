namespace WundergroundApi.Api;

public interface IWundergroundApi
{
    public Task<ObservationResponseDto> GetPersonalWeatherStationCurrentObservationAsync(
        CancellationToken cancellationToken = default);
}