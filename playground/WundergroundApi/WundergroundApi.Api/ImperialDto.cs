using System.Text;
using System.Text.Json.Serialization;

namespace WundergroundApi.Api;

public class ImperialDto
{
  [JsonPropertyName("temp")]
  public int Temperature { get; set; }

  [JsonPropertyName("heatIndex")]
  public int HeatIndex { get; set; }

  [JsonPropertyName("dewpt")]
  public int DewPoint { get; set; }

  [JsonPropertyName("windChill")]
  public int WindChill { get; set; }

  [JsonPropertyName("windSpeed")]
  public int WindSpeed { get; set; }

  [JsonPropertyName("windGust")]
  public int WindGust { get; set; }

  [JsonPropertyName("pressure")]
  public double Pressure { get; set; }

  [JsonPropertyName("precipRate")]
  public double PrecipitationRate { get; set; }

  [JsonPropertyName("precipTotal")]
  public double PrecipitationTotal { get; set; }

  [JsonPropertyName("elev")]
  public int Elevation { get; set; }

  public override string ToString()
  {
    var table = new StringBuilder();
    table.AppendLine("Imperial Readings");
    table.AppendLine("---------------------");
    table.AppendLine($"Temperature:        {Temperature}°F");
    table.AppendLine($"Heat Index:         {HeatIndex}°F");
    table.AppendLine($"Dew Point:          {DewPoint}°F");
    table.AppendLine($"Wind Chill:         {WindChill}°F");
    table.AppendLine($"Wind Speed:         {WindSpeed} mph");
    table.AppendLine($"Wind Gust:          {WindGust} mph");
    table.AppendLine($"Pressure:           {Pressure} inHg");
    table.AppendLine($"Precipitation Rate: {PrecipitationRate} in/hr");
    table.AppendLine($"Precipitation Total:{PrecipitationTotal} in");
    table.AppendLine($"Elevation:          {Elevation} feet");
    return table.ToString();
  }
}