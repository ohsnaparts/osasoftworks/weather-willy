﻿using System.Text.Json;
using WundergroundApi.Api.Extensions;

namespace WundergroundApi.Api;

public class WundergroundApi(
    HttpClient httpClient,
    string apiKey,
    string personalWeatherStationId
) : IWundergroundApi
{
    private static readonly string ApiBaseUrl = "https://api.weather.com";

    private readonly string _apiUrlPersonalWeatherStationCurrentObservation =
        $"{ApiBaseUrl}/v2/pws/observations/current?stationId={personalWeatherStationId}&format=json&units=e&apiKey={apiKey}";


    public async Task<ObservationResponseDto> GetPersonalWeatherStationCurrentObservationAsync(
        CancellationToken cancellationToken = default
    )
    {
        var response = await InvokeGetPersonalWeatherStationCurrentObservationRequestAsync(cancellationToken);
        response.EnsureSuccessStatusCode();
        
        var responseBody = await response.Content.ReadAsStringAsync();
        var responseDto = JsonSerializer.Deserialize<ObservationResponseDto>(responseBody);
        responseDto.ThrowIfNull(nameof(responseDto));
        
        return responseDto!;
    }

    private Task<HttpResponseMessage> InvokeGetPersonalWeatherStationCurrentObservationRequestAsync(
        CancellationToken cancellationToken
    ) => httpClient.GetAsync(_apiUrlPersonalWeatherStationCurrentObservation, cancellationToken);
}