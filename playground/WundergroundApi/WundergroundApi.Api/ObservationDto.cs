using System.Text;
using System.Text.Json.Serialization;

namespace WundergroundApi.Api;

public class ObservationDto
{
  [JsonPropertyName("stationID")]
  public string StationID { get; set; } = string.Empty;

  [JsonPropertyName("obsTimeUtc")]
  public DateTime ObservationTimeUtc { get; set; } = DateTime.MinValue;

  [JsonPropertyName("obsTimeLocal")]
  public string ObservationTimeLocal { get; set; } = string.Empty;

  [JsonPropertyName("neighborhood")]
  public string Neighborhood { get; set; } = string.Empty;

  [JsonPropertyName("softwareType")]
  public string SoftwareType { get; set; } = string.Empty;

  [JsonPropertyName("country")]
  public string Country { get; set; } = string.Empty;

  [JsonPropertyName("solarRadiation")]
  public double SolarRadiation { get; set; }

  [JsonPropertyName("lon")]
  public double Longitude { get; set; }

  [JsonPropertyName("realtimeFrequency")]
  public string RealtimeFrequency { get; set; } = string.Empty;

  [JsonPropertyName("epoch")]
  public long Epoch { get; set; }

  [JsonPropertyName("lat")]
  public double Latitude { get; set; }

  [JsonPropertyName("ultravioletIndex")]
  public double UltravioletIndex { get; set; }

  [JsonPropertyName("winddir")]
  public int WindDirection { get; set; }

  [JsonPropertyName("humidity")]
  public int Humidity { get; set; }

  [JsonPropertyName("qcStatus")]
  public int QualityControlStatus { get; set; }

  [JsonPropertyName("imperial")]
  public ImperialDto Imperial { get; set; } = new ImperialDto();

  public override string ToString()
    {
        var table = new StringBuilder();
        table.AppendLine("Observation Details");
        table.AppendLine("---------------------");
        table.AppendLine($"Station ID:              {StationID}");
        table.AppendLine($"Observation Time UTC:    {ObservationTimeUtc}");
        table.AppendLine($"Observation Time Local:  {ObservationTimeLocal}");
        table.AppendLine($"Neighborhood:            {Neighborhood}");
        table.AppendLine($"Software Type:           {SoftwareType}");
        table.AppendLine($"Country:                 {Country}");
        table.AppendLine($"Solar Radiation:         {SolarRadiation} W/m²");
        table.AppendLine($"Longitude:               {Longitude}");
        table.AppendLine($"Realtime Frequency:      {RealtimeFrequency}");
        table.AppendLine($"Epoch:                   {Epoch}");
        table.AppendLine($"Latitude:                {Latitude}");
        table.AppendLine($"Ultraviolet Index (UV):  {UltravioletIndex}");
        table.AppendLine($"Wind Direction:          {WindDirection}°");
        table.AppendLine($"Humidity:                {Humidity}%");
        table.AppendLine($"Quality Control:         {QualityControlStatus}");
        table.AppendLine(Imperial.ToString()); // Print Imperial readings in a nested table
        return table.ToString();
    }
}
