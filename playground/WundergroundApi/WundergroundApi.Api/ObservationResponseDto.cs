using System.Text.Json.Serialization;

namespace WundergroundApi.Api;

public class ObservationResponseDto
{
    [JsonPropertyName("observations")] public ObservationDto[] Observations { get; set; } = Array.Empty<ObservationDto>();

}