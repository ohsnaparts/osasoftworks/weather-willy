
using Meadow.Foundation;

internal static class ColorExtensions
{
    public static byte[] ToByteArray(this Color color) => new[] {
            color.R,
            color.G,
            color.B
            };

    public static Color ToColor(this byte[] bytes) => Color.FromRgb(bytes[0], bytes[1], bytes[2]);
    public static string ToRgbString(this Color color) => string.Join(',',color.ToByteArray());
}