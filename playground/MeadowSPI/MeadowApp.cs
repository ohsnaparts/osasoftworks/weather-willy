﻿using Meadow;
using Meadow.Devices;
using Meadow.Foundation;
using Meadow.Foundation.Leds;
using Meadow.Hardware;
using Meadow.Peripherals.Leds;
using System;
using System.Threading.Tasks;

namespace MeadowApp
{
    public class MeadowApp : App<F7FeatherV1>
    {
        private RgbPwmLed _onboardLed;
        private ISpiBus _spiBus;

        public override Task Initialize()
        {
            Resolver.Log.Info("Initialize...");
            _spiBus = Device.CreateSpiBus();



            _onboardLed = new RgbPwmLed(
                redPwmPin: Device.Pins.OnboardLedRed,
                greenPwmPin: Device.Pins.OnboardLedGreen,
                bluePwmPin: Device.Pins.OnboardLedBlue,
                CommonType.CommonAnode);

            return base.Initialize();
        }

        public override async Task Run()
        {
            Resolver.Log.Info("Run...");

            IPin spiSlaveSelect = Device.Pins.D00;
            IDigitalOutputPort spiPeriphChipSelect = Device.CreateDigitalOutputPort(spiSlaveSelect);

            for(uint i = 0; i < 100; i++) {
                var color = this.writeAndReadColor(spiPeriphChipSelect);
                await ShowColorPulse(color, TimeSpan.FromSeconds(2));
            }            
        }

        private Color writeAndReadColor(IDigitalOutputPort slaveSelectPort) {
            byte[] rgbBufferIn = new byte[3];
            Color colorOut = getRandomColor();

            _spiBus.Exchange(slaveSelectPort, colorOut.ToByteArray(), rgbBufferIn);

            Color colorIn = rgbBufferIn.ToColor();
            Resolver.Log.Info($"Sent color {colorOut.ToRgbString()} => received {colorIn.ToRgbString()}");

            return colorIn;
        }

        private Color getRandomColor() {
            byte[] rgb = new byte[3];
            new Random().NextBytes(rgb);
            return Color.FromRgb(rgb[0], rgb[1], rgb[2]);
        }


        async Task ShowColorPulse(Color color, TimeSpan duration)
        {
            await _onboardLed.StartPulse(color, duration / 2);
            await Task.Delay(duration);
        }
    }
}