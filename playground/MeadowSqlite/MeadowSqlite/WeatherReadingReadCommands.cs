using System.Data;
using Dapper;
using Meadow.Units;
using Microsoft.Data.Sqlite;

namespace MeadowSqlite;

public static class WeatherReadingReadCommands
{
    public static Task<IEnumerable<WeatherReading>> GetAllAsync(
        SqliteConnection databaseConnection,
        CancellationToken cancellationToken = default
    ) => databaseConnection.QueryAsync<WeatherReading>(
        WeatherReadingReadQueries.GetAll,
        cancellationToken);
}