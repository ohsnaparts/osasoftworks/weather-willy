﻿using System.Data;
using Meadow;
using Meadow.Pinouts;
using Meadow.Units;
using Microsoft.Data.Sqlite;

namespace MeadowSqlite;

public class MeadowApp : App<Linux<RaspberryPi>>
{
    static async Task Main(string[] args)
    {
        await MeadowOS.Start(args);
    }

    public override Task Initialize()
    {
        Resolver.Log.Info("Initialize...");

        return base.Initialize();
    }

    public override async Task Run()
    {
        Resolver.Log.Info("Run...");
        Resolver.Log.Info("Hello, Meadow.Linux SQLite!");
        
        var databasePath = Path.Combine(MeadowOS.FileSystem.DataDirectory, "MySqliteDatabase.db");
        var connectionString = $"Data Source={databasePath}";

        Resolver.Log.Info($"Connecting to database: {connectionString}");
        await using var databaseConnection = new SqliteConnection(connectionString);
        
        await databaseConnection.OpenAsync();
        
        Resolver.Log.Info($"Scaffolding database schema...");
        await WeatherReadingWriteCommands.CreateTableIfExistsAsync(databaseConnection);
        Resolver.Log.Info($"Inserting fake reading...");
        await WeatherReadingWriteCommands.InsertReadingAsync(databaseConnection, NewRandomWeatherReading());
        Resolver.Log.Info($"Persisted readings:");
        await LogReadingsAsync(databaseConnection);

        await base.Run();
    }

    private static WeatherReading NewRandomWeatherReading()
    {
        return new WeatherReading(
            null,
            DateTimeOffset.Now.ToUnixTimeSeconds(),
            Random.Shared.Next() % 40
        );
    }

    private static async Task LogReadingsAsync(SqliteConnection databaseConnection)
    {
        foreach (var weatherReading in await WeatherReadingReadCommands.GetAllAsync(databaseConnection))
        {
            Resolver.Log.Info(weatherReading.ToString()!);
        }
    }
}