namespace MeadowSqlite;

public static class WeatherReadingReadQueries
{
    public static string GetAll = "SELECT Id, TimestampSeconds, Temperature FROM WeatherReading";
}