namespace MeadowSqlite;

public record WeatherReading(long? Id, long TimestampSeconds, long Temperature);