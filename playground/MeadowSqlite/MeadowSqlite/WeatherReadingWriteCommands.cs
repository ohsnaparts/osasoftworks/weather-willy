using System.Data.Common;
using Dapper;
using Microsoft.Data.Sqlite;

namespace MeadowSqlite;

public static class WeatherReadingWriteCommands
{
    public static Task InsertReadingAsync(
        SqliteConnection databaseConnection,
        WeatherReading reading
    ) => databaseConnection.ExecuteAsync(
        WeatherReadingWriteQueries.InsertReading,
        reading);

    public static Task CreateTableIfExistsAsync(DbConnection databaseConnection) =>
        databaseConnection.ExecuteAsync(WeatherReadingWriteQueries.CreateTableIfExists);
}