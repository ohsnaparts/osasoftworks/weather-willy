namespace MeadowSqlite;

public static class WeatherReadingWriteQueries
{
    public static string InsertReading = """
                                             INSERT INTO WeatherReading(Id, TimestampSeconds, Temperature)
                                             VALUES (NULL, @TimestampSeconds, @Temperature);
                                         """;

    public static string CreateTableIfExists = """
                                           CREATE TABLE IF NOT EXISTS WeatherReading (
                                               Id INTEGER PRIMARY KEY AUTOINCREMENT,
                                               TimestampSeconds INTEGER,
                                               Temperature INTEGER
                                           );
                                       """;
}