# Meadow SQLite

Playground for testing native / embedded SQLite support

## Run

```pwsh
pwsh ./run.ps1
# Ctrl+C
```

![gif showing a full build and execution run](./screenshots/termtosvg_9ou3sg8h.svg)