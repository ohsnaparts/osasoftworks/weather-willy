#include <SPI.h>

#define SPI_PUBLISH_INTERVAL_MS 0
#define MHZ_24 24000000
#define MHZ_4 4000000
#define SERIAL_BAUD_RATE 9600
#define SPI_FREQUENCY MHZ_4
#define SPI_CHIP_SELECT_PIN 10


/**
 * @brief this method will send a character to SPI and immediately read
 *        the data that is currently on the MISO line (data in).
 * @returns the data that is currently present on the MISO line
 * @param character this character is sent as payload to the selected SPI target
 * @param slave_select_pin data is being sent to the SPI peripheral connected to this pin
*/
char spi_send_and_receive_char(char slave_select_pin, char character) {
  spi_select_slave(slave_select_pin);
    Serial.print(" Transfer:");
    Serial.print(character);
    char received = SPI.transfer(character);
  
    Serial.print(" Read:");
    Serial.print(received);
    Serial.print(" ");
  spi_deselect_slave(slave_select_pin);

  return received;
}

/** 
 * @brief Configures device as SPI master with default values
 * @details Articles and blogs >assume< these pins to be correct default
*     SS:   10
*     MOSI: 11
*     MISO: 12
*     SCK:  13
*  but please keep in mind that older devices might have different setups.
*  For instance, the Leonardo ETH model uses the SPI pins directly off the ICSP header.
*  More up to date models connect these to digital pins, yet some older models, such as
*  the Leonardo ETH do not seem to do this. Keep that in mind when troubleshooting SPI
*  communications.
* @see https://arduino.stackexchange.com/a/40099
*/
void init_spi() {
  Serial.println("Initializing SPI...");
  SPI.begin();
}

void await_serial_readyness() {
  while(!Serial) {
    delay(10);
  }
}

void init_serial() {
  Serial.begin(SERIAL_BAUD_RATE);
  await_serial_readyness();
}


/**
 * @brief begins a new SPI transaction with application wide default values
 */
void spi_begin_transaction() {
  SPI.beginTransaction(SPISettings(SPI_FREQUENCY, MSBFIRST, SPI_MODE1));
}

/**
 * @brief counter part of @ref spi_begin_transaction
 */
void spi_end_transaction() {
  SPI.endTransaction();
}

/**
 * @brief convenience function to select a particular SPI peripheral as target
 * @details only after calling this method, will data sent over SPI actually be
 *          received by the targeted peripheral.
 * @note "Slave Select (SS)" is also being called "Chip Select (CS)"
 * @param slave_select_pin data is being sent to the SPI peripheral connected to this pin
 */
void spi_select_slave(uint8_t slave_select_pin) {
  Serial.print(" CS:");
  Serial.print(slave_select_pin);
  Serial.print(":LOW ");
  digitalWrite(slave_select_pin, LOW);
}

void spi_deselect_slave(uint8_t slave_select_pin) {
  digitalWrite(slave_select_pin, HIGH);
  Serial.print(" CS:");
  Serial.print(slave_select_pin);
  Serial.print(":HIGH ");
}


// =====================================================================

/**
 * @brief Arduino setup function
 * @details This function is called only once after microcontroller boot
 */
void setup() {
  init_serial();
  init_spi();
  
}

/**
 * @brief Sends and receives the alphabet over a loopback SPI interface
 * @details To make this sample work, you will have to create a loopback
 *          between the MOSI (data out) and MISO (data in) by connecting
 *          both via dupont wires. This will essentially simulate an SPI
 *          peripheral that echos every input command.
 * @see take extra care to read the docs for the `init_spi` function
*/
void loop() {
  Serial.println("loop...");

  //spi_begin_transaction();
  for(char c = 'A'; c < 'z'; c++) {
    spi_send_and_receive_char(SPI_CHIP_SELECT_PIN, c);

    Serial.print(" Waiting:");
    Serial.println(SPI_PUBLISH_INTERVAL_MS);
    delay(SPI_PUBLISH_INTERVAL_MS);
  }
  //spi_end_transaction();
}