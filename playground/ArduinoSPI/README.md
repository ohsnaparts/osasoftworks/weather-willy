# Arduino SPI

This sample validates the workings of SPI using an Arduino
Leonardo ETH board.

It uses a loopback connection from `MOSI` to `MISO` so no additional controller is required.

![](./screenshots//arduino.jpeg)
![](./screenshots/arduino-leonardo-pinout-1536x1086.jpg)

## Technologies

* Arduino
* SPI
* C

## Run

1. Download Arduino IDE
1. Select Leonardo ETH port
1. Upload sketch
1. Open Serial Monitor

```txt
Message (Enter to send message to 'Arduino Leonardo ETH' on '/dev/ttyACM0')
New Line
9600 baud
22:59:00.427 -> loop...
22:59:00.427 ->  CS:LOW  Transfer:A Read:A  CS:HIGH  Waiting:1000
22:59:01.427 ->  CS:LOW  Transfer:B Read:B  CS:HIGH  Waiting:1000
22:59:02.426 ->  CS:LOW  Transfer:C Read:C  CS:HIGH  Waiting:1000
22:59:03.392 ->  CS:LOW  Transfer:D Read:D  CS:HIGH  Waiting:1000
...
22:59:39.298 ->  CS:LOW  Transfer:[ Read:[  CS:HIGH  Waiting:1000
22:59:40.298 ->  CS:LOW  Transfer:\ Read:\  CS:HIGH  Waiting:1000
22:59:41.297 ->  CS:LOW  Transfer:] Read:]  CS:HIGH  Waiting:1000
22:59:42.293 ->  CS:LOW  Transfer:^ Read:^  CS:HIGH  Waiting:1000
...
22:59:52.247 ->  CS:LOW  Transfer:w Read:w  CS:HIGH  Waiting:1000
22:59:53.245 ->  CS:LOW  Transfer:x Read:x  CS:HIGH  Waiting:1000
22:59:50.251 ->  CS:LOW  Transfer:y Read:y  CS:HIGH  Waiting:1000
22:59:51.251 ->  CS:LOW  Transfer:z Read:z  CS:HIGH  Waiting:1000
```