using Newtonsoft.Json.Linq;
using Meadow.Logging;


namespace MeadowHttp.Http
{
    public class GetTemperatureRequest
    {
        /// <summary>
        /// Creates an echo request that we can use as fake weather station API
        /// 
        /// In case the URL goes offline at some point, the expected format is
        /// {
        ///  "args": {
        ///    "temperature": "78",
        ///    "unit": "celsius"
        ///  },
        ///  "headers": {
        ///    "x-forwarded-proto": "https",
        ///    "x-forwarded-port": "443",
        ///    "host": "postman-echo.com",
        ///    "x-amzn-trace-id": "Root=1-657c38fa-3760ae8857b582df39aefe2e",
        ///    "user-agent": "Mozilla/5.0 (X11; Linux x86_64; rv:120.0) Gecko/20100101 Firefox/120.0",
        ///    "accept": "text/html,application/xhtml+xml,application/xml;q=0.9,image/avif,image/webp,*/*;q=0.8",
        ///    "accept-language": "en,en-US;q=0.8,en-GB;q=0.5,de;q=0.3",
        ///    "accept-encoding": "gzip, deflate, br",
        ///    "dnt": "1",
        ///    "upgrade-insecure-requests": "1",
        ///    "sec-fetch-dest": "document",
        ///    "sec-fetch-mode": "navigate",
        ///    "sec-fetch-site": "none",
        ///    "sec-fetch-user": "?1",
        ///    "sec-gpc": "1",
        ///    "cookie": "sails.sid=s%3AukiFHeGNOSpeNYXvjPUY_Z6XZ82f9w0x.8PJYINXL4lIzzkiDV57IbFK99f1BAlARgCubRLY7KCY"
        ///  },
        ///  "url": "https://postman-echo.com/get?temperature=78&unit=celsius"
        //}/
        /// </summary>
        private string WeatherApiUrl => $"https://postman-echo.com/get?temperature={new Random().Next(0, 100)}&unit=fahrenheit";
        private readonly HttpClient _httpClient;
        private readonly Logger _logger;

        public GetTemperatureRequest(HttpClient httpClient, Logger logger)
        {
            this._httpClient = httpClient;
            this._logger = logger;
        }

        public async Task<int> GetTemperatureAsync(CancellationToken cancellationToken = default)
        {
            this._logger.Info($"Issuing request: {WeatherApiUrl}");
            var response = await this._httpClient.GetAsync(
                new Uri(WeatherApiUrl),
                cancellationToken
            );

            var responseBody = await response.Content.ReadAsStringAsync();
            this._logger.Debug(responseBody);

            response.EnsureSuccessStatusCode();

            return GetResponseTemperature(responseBody) ?? 0;
        }

        private int? GetResponseTemperature(string response)
        {
            var jsonObject = JObject.Parse(response);
            var temperatureToken = jsonObject.SelectToken("args.temperature");
            return temperatureToken != null ? (int)temperatureToken : null;
        }
    }
}