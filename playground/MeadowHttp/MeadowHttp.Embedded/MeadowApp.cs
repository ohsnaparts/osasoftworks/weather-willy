﻿using Meadow;
using Meadow.Devices;
using Meadow.Foundation;
using Meadow.Foundation.Leds;
using Meadow.Gateway.WiFi;
using Meadow.Hardware;
using Meadow.Logging;
using Meadow.Peripherals.Leds;
using System;
using System.Net.Http;
using System.Threading;
using System.Threading.Tasks;
using MeadowHttp.Http;
using Meadow.Units;
using System.Globalization;

namespace MeadowHttp.Embedded
{
    public class MeadowApp : App<F7FeatherV1>
    {
        private IWiFiNetworkAdapter? _wifi;
        private Logger _logger;

        public override async Task Initialize()
        {
            Resolver.Log.Info("Initialize...");
            this._wifi = Device.NetworkAdapters.Primary<IWiFiNetworkAdapter>();
            this._logger = Resolver.Log;

            if (this._wifi == null)
            {
                throw new ArgumentNullException(nameof(this._wifi));
            }

            await this.ScanForAccessPointsAsync(this._wifi!);

            this._wifi.NetworkConnected += (sender, args) =>
            {
                this._logger.Info($"Connected to gateway {args.Gateway} with IP {args.IpAddress}/{args.Subnet}");
            };
            
            await this.DelayAsync(10, "Giving the network interface some time to get an IP");
            await base.Initialize();
        }

        public override async Task Run()
        {
            Resolver.Log.Info("Run...");

            this.PrintDateAndTime();

            for (int i = 0; i < 10; i++)
            {
                await this.PrintCurrentTemperatureAsync();
                await this.DelayAsync(1, "Waiting some time to not spam the public API");
            }
        }

        private async Task ScanForAccessPointsAsync(IWiFiNetworkAdapter wifi)
        {
            this._logger.Info("Scanning access points.");
            var networks = await wifi.Scan();

            this._logger.Info($"Found {networks.Count} networks.");
            if (networks.Count < 0)
            {
                return;
            }

            this._logger.Info("|-------------------------------------------------------------|---------|");
            this._logger.Info("|         Network Name             | RSSI |       BSSID       | Channel |");
            this._logger.Info("|-------------------------------------------------------------|---------|");

            foreach (WifiNetwork accessPoint in networks)
            {
                this._logger.Info($"| {accessPoint.Ssid,-32} | {accessPoint.SignalDbStrength,4} | {accessPoint.Bssid,17} |   {accessPoint.ChannelCenterFrequency,3}   |");
            }
        }

        private async Task PrintCurrentTemperatureAsync(CancellationToken cancellationToken = default)
        {
            var httpClient = new HttpClient();
            // Some webservers reject get requests without a user-agent header
            httpClient.DefaultRequestHeaders.Add("User-Agent", "Other");

            var request = new GetTemperatureRequest(httpClient, this._logger);
            var fahrenheit = await request.GetTemperatureAsync(cancellationToken);
            var temperature = new Temperature(fahrenheit, Temperature.UnitType.Fahrenheit);

            this._logger.Info($"Current temperature: {temperature.Celsius} Celsius");
        }

        private void PrintDateAndTime()
        {
            var iso8601 = DateTime.Now.ToString("yyyy-MM-ddTHH\\:mm\\:ss.fffffffzzz", CultureInfo.InvariantCulture);
            this._logger.Info(iso8601);
        }

        private Task DelayAsync(int seconds, string reason) => DelayAsync(TimeSpan.FromSeconds(seconds), reason);
        private Task DelayAsync(TimeSpan time, string reason) {
            this._logger.Info($"Waiting {time.TotalSeconds}s: {reason}");
            return Task.Delay(time);
        }
    }
}