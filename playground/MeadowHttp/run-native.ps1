$BuildConfiguration = 'Debug'.ToLower();
$ProjectDirPath = Join-Path $PSScriptRoot 'MeadowHttp.Native/'

dotnet run --configuration $BuildConfiguration --project $ProjectDirPath
