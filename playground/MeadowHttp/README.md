# Meadow Http

This project demonstrates how to handle HTTP requests using the meadow microcontroller

## Technologies

- WildernessLabs Meadow Feature v1.d
- Meadow WIFI connection
- Meadow Http Request

## Requirements

- Wilderness Labs Meadow Micro v1.d
  - Thats the development board
  - It should work the same with any other meadow board
- powershell
- Wifi Credentials setup

## Run

- Configure your wifi credentials => [wifi.config.yaml]
  - Don't forget to remove it when finished :)
  - A list of accessible wifi accesspoints is printed at startup
- Plug in Microcontroller

```pwsh
pwsh ./run-native.sh
pwsh ./run-embedded.sh
```

```
Connecting to Meadow on /dev/ttyACM0

Meadow StdOut: Device is configured to use WiFi for the network interface
Listening for Meadow Console output. Press Ctrl+C to exit
Meadow StdOut: Update Service is disabled.
Meadow StdOut: Health Metrics disabled.
Meadow StdOut: Initialize...
Meadow StdOut: Scanning access points.
Meadow StdOut: Found 5 networks.
Meadow StdOut: |-------------------------------------------------------------|---------|
Meadow StdOut: |         Network Name             | RSSI |       BSSID       | Channel |
Meadow StdOut: |-------------------------------------------------------------|---------|
Meadow StdOut: | Vos                              |  -57 |      F0DCB2AD6F0D |    11   |
Meadow StdOut: | TrainNetwork                     |  -76 |      F01C7ADBF603 |     4   |
Meadow StdOut: | TAT-WLAN                         |  -79 |      10A29E2A3FE1 |    11   |
Meadow StdOut: | GTS-Intern                       |  -79 |      10ACC92A36E3 |    11   |
Meadow StdOut: | Isengard                         |  -83 |      10FC7223FF60 |    11   |
Meadow StdOut: Run...
Meadow StdOut: 2023-12-15T11:27:10.8983520+00:00
Meadow StdOut: Issuing request: https://postman-echo.com/get?temperature=95&unit=fahrenheit
Meadow StdOut: Connected to gateway 255.255.255.255 with IP 172.20.10.14/255.255.255.240
Meadow StdOut: Current temperature: -58.3333333333333 Celsius
Meadow StdOut: Current temperature: -28.8888888888889 Celsius
Meadow StdOut: Current temperature: -63.8888888888889 Celsius
Meadow StdOut: Current temperature: 11.6666666666667 Celsius
```

[wifi.config.yaml]: ./MeadowHttp.Embedded/wifi.config.yaml
