﻿using Meadow;
using Meadow.Logging;
using Meadow.Pinouts;
using Meadow.Units;
using MeadowHttp.Http;

public class MeadowApp : App<Linux<WSL2>>
{
    private Logger _logger;


    static async Task Main(string[] args)
    {
        await MeadowOS.Start(args);
    }

    public override async Task Initialize()
    {
        Resolver.Log.Info("Initialize...");
        this._logger = Resolver.Log;

        await base.Initialize();
    }

    public override async Task Run()
    {
        this._logger.Info("Run...");

        this._logger.Info("Hello, Meadow.Linux!");

        for(var i = 0; i < 10; i++) {
            await PrintCurrentTemperatureAsync();
            await Task.Delay(TimeSpan.FromSeconds(1));
        }
        
        await base.Run();
    }

    private async Task PrintCurrentTemperatureAsync(CancellationToken cancellationToken = default)
    {
        // https://developer.wildernesslabs.co/Meadow/Meadow.OS/Networking/#performing-requests
        var httpClient = new HttpClient();
        // Some webservers reject get requests without a user-agent header
        httpClient.DefaultRequestHeaders.Add("User-Agent", "Other");
        
        var request = new GetTemperatureRequest(httpClient, this._logger);
        var fahrenheit = await request.GetTemperatureAsync(cancellationToken);
        var temperature = new Temperature(fahrenheit, Temperature.UnitType.Fahrenheit);
        
        this._logger.Info($"Current temperature: {temperature.Celsius} Celsius");
    }
}