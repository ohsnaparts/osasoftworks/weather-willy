[CmdletBinding()]
param (
    [Parameter()]
    [ValidateSet('debug', 'release')]
    [string] $BuildConfiguration = 'debug',
    # -------------------------------------
    [Parameter()]
    [string] $CsprojPath = (Join-Path $PSScriptRoot 'MeadowHttp.Embedded/MeadowHttp.Embedded.csproj'),
    # -------------------------------------
    [Parameter()]
    [string] $ApplicationMainDll = 'App.dll'
)

$SolutionRoot = $PSScriptRoot
$RepositoryRoot = Join-Path -Resolve $SolutionRoot '../..'
$ScriptsDirPath = Join-Path -Resolve $RepositoryRoot 'scripts'
$ProjectName = Split-Path -Leaf $CsprojPath
$BuildsDirPath = Join-Path $SolutionRoot "bin/$ProjectName/$BuildConfiguration"

# constants
$SuccessErrorCode = 0

. (Join-Path $ScriptsDirPath 'Invoke-MeadowCommand.ps1')

$OutputColor = @{
    ForegroundColor = [System.ConsoleColor]::Green
}


Write-Host @OutputColor "Building application in $BuildConfiguration mode"
dotnet build $CsprojPath --configuration $BuildConfiguration --output $BuildsDirPath
if($LASTEXITCODE -ne $SuccessErrorCode) {
    exit $LASTEXITCODE
}

Push-Location $BuildsDirPath
Try {
    Invoke-MeadowCommand -ErrorAction Stop -ScriptBlock {
        Write-Host @OutputColor "Deploying $ApplicationMainDll"
        meadow app deploy --file $ApplicationMainDll
    }
} Finally {
    Pop-Location
}

Write-Host @OutputColor "Listening to serial outputs..."
Invoke-MeadowCommand -ErrorAction Stop -ScriptBlock {
    meadow listen
}
