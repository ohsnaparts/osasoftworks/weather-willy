# Playground

This folder contains projects meant as a safe space for experimenting with certain tech and topics. 

> These projects are meant for developing.\
If you are looking for application examples, see folder _[/examples][examples]_

## Rules

Each project is linked here and contains a README containing

1. Short description
   - What does it try to solve
1. Short overview on used tech and important decisions
   - Primarily to make these projects searchable

## Projects

* [GTK Display](/playground/MeadowDesktopGtk)

[examples]: /examples/