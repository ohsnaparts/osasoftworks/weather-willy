using WeatherWilly.Common.Configuration;

namespace WeatherWilly.Common.Tests.Configuration
{

    public class WeatherApiTestSettings : IWeatherApiReadSettings
    {
        public TimeSpan UpdateInterval { get; set; }
        public string ApiKey { get; set; }
        public string WeatherStationId => throw new NotImplementedException();
    }
}