using WeatherWilly.Common.Configuration;

namespace WeatherWilly.Common.Tests.Configuration
{
    public class DisplayTestSettings : IDisplayReadSettings
    {
        public TimeSpan MinSafeRenderInterval { get; set; }
        public int Width { get; set; }
        public int Height { get; set; }
        public bool SupportsColor { get; set; }
        public int SpiFrequencyHz { get; set; }
        public string PinSpiChipSelect { get; set; }
        public string PinSpiClock { get; set; }
        public string PinSpiMiso { get; set; }
        public string PinSpiMosi { get; set; }
        public string PinDataCommand { get; set; }
        public string PinReset { get; set; }
        public string PinBusy { get; set; }
        public string PinDisplaySramChipSelect { get; set; }
        public TimeSpan FullRefreshInterval { get; set; }
    }
}