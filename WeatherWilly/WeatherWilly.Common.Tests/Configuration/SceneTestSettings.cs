using WeatherWilly.Common.Configuration;

namespace WeatherWilly.Common.Tests.Configuration
{
    public class SceneTestSettings : ISceneReadSettings
    {
        public string BackgroundImagePath { get; set; }
        public bool DebugMode { get; set; }
    }
}