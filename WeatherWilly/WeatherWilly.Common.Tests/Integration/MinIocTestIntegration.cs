using FluentAssertions;
using Microsoft.Extensions.DependencyInjection;
using WeatherWilly.Common.Configuration;
using WeatherWilly.Common.IoC.MinIoC;
using WeatherWilly.Common.IoC.Modules;
using WeatherWilly.Common.Service;

namespace WeatherWilly.Common.Tests;

public class MinIocTestIntegration
{
    private readonly IServiceProvider services;

    public MinIocTestIntegration()
    {
        this.services = new RootIocModule()
            .Register(
                new CommonIocModule(),
                new WeatherWillyIocModule()
            ).Finalize();
    }

    [Theory]
    [InlineData(typeof(IDisplayReadSettings))]
    [InlineData(typeof(ISceneReadSettings))]
    [InlineData(typeof(IDelayService))]
    [InlineData(typeof(Container))]
    [InlineData(typeof(IServiceCollection))]
    [InlineData(typeof(IServiceProvider))]
    public void ContainerResolves(Type type)
    {
        var @object = this.services.GetRequiredService(type);
        @object.Should().NotBeNull();
    }
}