using WeatherWilly.Common.Tests.Integration;
using WeatherWilly.Common.IoC;
using WeatherWilly.Common.IoC.MinIoC;
using Microsoft.Extensions.DependencyInjection;


namespace WeatherWilly.Common.Tests;

public class ContainerServiceCollectionExtensionsIntegrationTest : ContainerExtensionsTestBase
{
    public static IEnumerable<object[]> GetTransientServiceCollectionRegistrators() => GetRegistrators<IServiceCollection>(
        collection => collection.AddTransient<IMathService, MathService>(),
        collection => collection.AddTransient<IMathService>(services => new MathService())
    );

    public static IEnumerable<object[]> GetScopedServiceCollectionRegistrators() => GetRegistrators<IServiceCollection>(
        collection => collection.AddScoped<IMathService, MathService>(),
        collection => collection.AddScoped<IMathService>(services => new MathService())
    );

    public static IEnumerable<object[]> GetSingletonServiceCollectionRegistrators() => GetRegistrators<IServiceCollection>(
        collection => collection.AddSingleton<IMathService, MathService>(),
        collection => collection.AddSingleton<IMathService>(services => new MathService()),
        Collection => Collection.AddSingleton<IMathService>(new MathService())
    );

    [Theory]
    [MemberData(nameof(GetTransientServiceCollectionRegistrators), MemberType = typeof(ContainerServiceCollectionExtensionsIntegrationTest))]
    public void FromServiceCollection_WithTransientRegistrations_ProducesExpectedService(Action<IServiceCollection> registrator)
    {
        var serviceCollection = new ServiceCollection().UsingRegistrator(registrator);
        var container = new Container().FromServiceCollection(serviceCollection);
        AssertTransientServiceRegistration(container);
    }

    [Theory]
    [MemberData(nameof(GetScopedServiceCollectionRegistrators), MemberType = typeof(ContainerServiceCollectionExtensionsIntegrationTest))]
    public void FromServiceCollection_WithScopedRegistrations_ProducesExpectedService(Action<IServiceCollection> registrator)
    {
        var serviceCollection = new ServiceCollection().UsingRegistrator(registrator);
        var container = new Container().FromServiceCollection(serviceCollection);
        AssertScopedServiceRegistration(container);
    }

    [Theory]
    [MemberData(nameof(GetSingletonServiceCollectionRegistrators), MemberType = typeof(ContainerServiceCollectionExtensionsIntegrationTest))]
    public void FromServiceCollection_WithSingletonRegistrations_ProducesExpectedService(Action<IServiceCollection> registrator)
    {
        var serviceCollection = new ServiceCollection().UsingRegistrator(registrator);
        var container = new Container().FromServiceCollection(serviceCollection);
        AssertSingletonServiceRegistration(container);
    }
}
