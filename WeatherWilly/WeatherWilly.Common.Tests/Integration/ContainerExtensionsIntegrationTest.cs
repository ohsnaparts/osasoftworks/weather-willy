using WeatherWilly.Common.Tests.Integration;
using WeatherWilly.Common.IoC;
using WeatherWilly.Common.IoC.MinIoC;
using Microsoft.Extensions.DependencyInjection;


namespace WeatherWilly.Common.Tests;


public class ContainerExtensionsIntegrationTest : ContainerExtensionsTestBase
{
    private readonly MathService _mathService;
    private readonly Container _container;

    public ContainerExtensionsIntegrationTest()
    {
        this._container = new Container();
        this._mathService = new MathService();
    }

    public static IEnumerable<object[]> GetTransientContainerRegistrators() => GetRegistrators<Container>(
        container => container.Register<IMathService, MathService>(ServiceLifetime.Transient),
        container => container.Register<IMathService>(() => new MathService()),
        container => container.Register<IMathService>((_) => new MathService())
    );

    public static IEnumerable<object[]> GetScopedContainerRegistrators() => GetRegistrators<Container>(
        container => container.Register<IMathService, MathService>(ServiceLifetime.Scoped),
        container => container.Register<IMathService>(() => new MathService()).PerScope(),
        container => container.Register<IMathService>((_) => new MathService()).PerScope()
    );

    public static IEnumerable<object[]> GetSingletonContainerRegistrators() => GetRegistrators<Container>(
        container => container.Register<IMathService, MathService>(ServiceLifetime.Singleton),
        container => container.Register<IMathService>(() => new MathService()).AsSingleton(),
        container => container.Register<IMathService>((_) => new MathService()).AsSingleton(),
        container => container.Register<IMathService>(new MathService())
    );

    [Fact]
    public void Register_Interface_Works()
    {
        var registerd = this._container.Register<IMathService>(() => new MathService());
        AssertTypeResolvable<IMathService>(this._container);
    }

    [Fact]
    public void Register_GivenThrowingObjectFactory_Succeeds()
    {
        var instance = new MathService();
        this._container.Register<IMathService>(instance);
        AssertTypeResolvesTo<IMathService>(this._container, instance);
    }

    [Fact]
    public void Register_GivenObjectFactory_Succeeds()
    {
        var instance = new MathService();
        this._container.Register<IMathService>(() => instance);
        AssertTypeResolvesTo<IMathService>(this._container, instance);
    }

    [Theory]
    [MemberData(nameof(GetTransientContainerRegistrators), MemberType = typeof(ContainerExtensionsIntegrationTest))]
    public void Register_WithTransientServiceLifeTime_ProducesDifferentInstances(Action<Container> registrator)
    {
        var container = new Container().UsingRegistrator(registrator);
        AssertTransientServiceRegistration(container);
    }

    [Theory]
    [MemberData(nameof(GetScopedContainerRegistrators), MemberType = typeof(ContainerExtensionsIntegrationTest))]
    public void Register_WithScopedServiceLifeTime_ProducesSameInstanceWithinSameScope(Action<Container> registrator)
    {
        var container = new Container().UsingRegistrator(registrator);
        AssertScopedServiceRegistration(container);
    }


    [Theory]
    [MemberData(nameof(GetSingletonContainerRegistrators), MemberType = typeof(ContainerExtensionsIntegrationTest))]
    public void Register_WithSingletonServiceLifeTime_ProducesSameInstance(Action<Container> registrator)
    {
        var container = new Container().UsingRegistrator(registrator);
        AssertSingletonServiceRegistration(container);
    }
}