using WeatherWilly.Common.Tests.Integration;
using WeatherWilly.Common.IoC.MinIoC;
using FluentAssertions;
using Microsoft.Extensions.DependencyInjection;


namespace WeatherWilly.Common.Tests;

public abstract class ContainerExtensionsTestBase
{
    protected static IEnumerable<object[]> GetRegistrators<TContainer>(
       params Action<TContainer>[] registrators
   ) => registrators.Select(r => new object[] { r });

    protected static void AssertTypeResolvable<TType>(IServiceProvider serviceProvider)
    {
        var instance = serviceProvider.GetService(typeof(TType));
        instance.Should().NotBeNull();
    }

    protected static void AssertTypeResolvesTo<TType>(IServiceProvider serviceProvider, TType expected)
    {
        var instance = serviceProvider.GetService(typeof(TType));
        instance.Should().BeSameAs(expected);
    }

    protected static void AssertScopedServiceRegistration(Container container)
    {
        var service1 = container.GetRequiredService<IMathService>();
        var service2 = container.GetRequiredService<IMathService>();
        service1.Should().BeSameAs(service2, "scoped registrations should procuce the same instance in global scope.");

        using var scope1 = container.CreateScope();
        var service3 = scope1.GetRequiredService<IMathService>();
        var service4 = scope1.GetRequiredService<IMathService>();
        service3.Should().BeSameAs(service4, "scoped registrations should produce the same instance within the same scope.");

        using var scope2 = container.CreateScope();
        var service5 = scope2.GetRequiredService<IMathService>();
        service5
            .Should()
            .NotBeSameAs(service4, "different scopes produce different instances.")
            .And
            .NotBeSameAs(service2, "different scopes produce different instances.");
    }

    protected static void AssertSingletonServiceRegistration(Container container)
    {
        var service1 = container.GetRequiredService<IMathService>();
        var service2 = container.GetRequiredService<IMathService>();
        using var scope = container.CreateScope();
        var service3 = container.GetRequiredService<IMathService>();

        service1
        .Should()
        .BeSameAs(service3, "singleton registrations should procuce the same instance everytime.")
        .And
        .BeSameAs(service2, "singleton registrations should procuce the same instance everytime.");
    }

    protected static void AssertTransientServiceRegistration(Container services)
    {
        var service1 = services.GetRequiredService<IMathService>();
        var service2 = services.GetRequiredService<IMathService>();
        service1.Should().NotBeSameAs(service2, "transient registrations should produce new instances every time.");
    }
}
