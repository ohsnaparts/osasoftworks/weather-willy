using System;

namespace WeatherWilly.Common.Tests.Integration;

internal interface IMathService
{
    int Add(int a, int b);
}

internal class MathService : IMathService
{
    public int Add(int a, int b) => a + b;
}