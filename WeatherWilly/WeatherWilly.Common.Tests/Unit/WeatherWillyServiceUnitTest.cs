﻿using Moq;
using Meadow.Logging;
using WeatherWilly.Common.Display;
using WeatherWilly.Common.Service;
using WeatherWilly.Common.Tests.Configuration;
using WeatherWilly.Common.Weather;

namespace WeatherWilly.Common.Tests;

public class WeatherWillyServiceUnitTest
{
    private readonly Mock<IDelayService> _delayServiceMock = new();
    private readonly Mock<IWundergroundApi> _weatherApiMock = new();
    private readonly Mock<IWeatherWillyScene> _weatherWillyScene = new();
    private readonly WeatherWillyService _weatherWilly;
    private readonly WeatherApiTestSettings _weatherApiSettings = new();
    private readonly DisplayTestSettings _displaySettings = new();
    private readonly Logger _logger;



    public WeatherWillyServiceUnitTest()
    {
        this._logger = new Logger(new ConsoleLogProvider());
        this._weatherWilly = new WeatherWillyService(
            this._logger,
            this._weatherApiSettings,
            this._displaySettings,
            this._delayServiceMock.Object,
            this._weatherApiMock.Object,
            this._weatherWillyScene.Object
        );

        this.SetupDelayServiceMock_DelayAsync();
        this.SetupWeatherApiMock_GetPersonalWeatherStationCurrentObservationAsync(
            new Weather.Models.ObservationResponseDto()
        );
        this.ConfigureWeatherApiSettings();
        this.ConfigureDisplaySettings();
    }


    private void SetupDelayServiceMock_DelayAsync() => this._delayServiceMock
        .Setup(s => s.DelayAsync(It.IsAny<TimeSpan>(), It.IsAny<CancellationToken>()))
        .Returns(Task.CompletedTask);

    private void SetupWeatherApiMock_GetPersonalWeatherStationCurrentObservationAsync(
        Weather.Models.ObservationResponseDto returnValue
    ) => this._weatherApiMock
        .Setup(w => w.GetPersonalWeatherStationCurrentObservationAsync(It.IsAny<CancellationToken>()))
        .Returns(Task.FromResult(returnValue));

    private void ConfigureWeatherApiSettings(Action<WeatherApiTestSettings>? configurator = null) => SetupSettings(
        this._weatherApiSettings,
        configurator
    );

    private void ConfigureDisplaySettings(Action<DisplayTestSettings>? configurator = null) => SetupSettings(
        this._displaySettings,
        configurator
    );


    private static void SetupSettings<TSetting>(
        TSetting settings,
        Action<TSetting>? configurator
    ) where TSetting : class, new()
    {
        if (configurator != null)
        {
            configurator(settings);
        }
    }
}