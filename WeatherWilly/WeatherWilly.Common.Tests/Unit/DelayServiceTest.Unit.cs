using FluentAssertions;
using Meadow.Logging;
using WeatherWilly.Common.Service;

namespace WeatherWilly.Common.Tests.Unit;

public class DelayServiceUnitTest
{
    private DelayService _delayService;

    public DelayServiceUnitTest()
    {
        this._delayService = new DelayService(new Logger(new ConsoleLogProvider()));
    }

    [Fact]
    public async Task Delay_SleepsAPeriodOfTimeAsync()
    {
        var timeToDelay = TimeSpan.FromSeconds(1.5);
        var timeBeforeDelay = DateTime.UtcNow;

        await this._delayService.DelayAsync(timeToDelay);

        var effectiveDelayTime = DateTime.UtcNow - timeBeforeDelay;
        effectiveDelayTime
            .Should()
            .BeCloseTo(timeToDelay, TimeSpan.FromMilliseconds(10));
    }

    [Theory]
    [InlineData(double.NaN)]
    [InlineData(-1)]
    public async Task Delay_ThrowsOnInvalidNumbers(double seconds)
    {
        var delayTask = new Func<Task>(() => this._delayService.DelayAsync(TimeSpan.FromSeconds(seconds)));
        await delayTask.Should().ThrowAsync<ArgumentException>();
    }

}