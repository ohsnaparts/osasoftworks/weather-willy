using System.Net;
using FluentAssertions;
using Meadow.Logging;
using Moq;
using Moq.Protected;
using WeatherWilly.Common.Configuration;
using WeatherWilly.Common.Service;

namespace WundergroundApi.Tests.Unit;

public class WundergroundApiTest
{
    [Fact]
    public async Task SuccessfullyResolvesApiResponseToDto()
    {
        var mockMessageHandler = MockHttpMessageHandler(WundergroundTestData.GetPwsCurrentObservation);
        var fakedHttpClient = new HttpClient(mockMessageHandler.Object);
        var httpFactory = MockHttpClientFactory(fakedHttpClient);
        var api = new WeatherWilly.Common.Weather.WundergroundApi(
            httpFactory.Object,
            new WeatherApiSettings(),
            new Logger(new ConsoleLogProvider())
        );

        var response = await api.GetPersonalWeatherStationCurrentObservationAsync(CancellationToken.None);
        response.Observations.Should().HaveCount(1);

        var observation = response.Observations.Single();
        observation.StationID.Should().NotBeNullOrEmpty();
        observation.ObservationTimeUtc.Should().NotBeNullOrEmpty();
        observation.ObservationTimeLocal.Should().NotBeNullOrEmpty();
        observation.Neighborhood.Should().NotBeNullOrEmpty();
        observation.SoftwareType.Should().NotBeNullOrEmpty();
        observation.Country.Should().NotBeNullOrEmpty();
        observation.SolarRadiation.Should().BeGreaterThan(0);
        observation.Longitude.Should().NotBe(0);
        observation.Epoch.Should().NotBe(0);
        observation.Latitude.Should().NotBe(0);
        observation.UltravioletIndex.Should().BeGreaterOrEqualTo(0);
        observation.WindDirection.Should().NotBe(0);
        observation.Humidity.Should().BeGreaterOrEqualTo(0);
        observation.QualityControlStatus.Should().BeInRange(0, 1);
        // unknowns / we don't have test data for this yet
        //observation.RealtimeFrequency.Should().NotBeNullOrEmpty();

        // Check ImperialDto
        observation.Imperial.Should().NotBeNull();
        observation.Imperial.Temperature.Should().NotBe(0);
        observation.Imperial.HeatIndex.Should().NotBe(0);
        observation.Imperial.DewPoint.Should().NotBe(0);
        observation.Imperial.WindChill.Should().NotBe(0);
        observation.Imperial.WindGust.Should().NotBe(0);
        observation.Imperial.Pressure.Should().BeGreaterThan(0);
        observation.Imperial.PrecipitationRate.Should().BeGreaterOrEqualTo(0);
        observation.Imperial.PrecipitationTotal.Should().BeGreaterOrEqualTo(0);
        observation.Imperial.Elevation.Should().BeGreaterOrEqualTo(0);
        // unknowns / we don't have test data for this yet
        // observation.Imperial.WindSpeed.Should().NotBe(0);
    }

    private static Mock<IHttpClientFactory> MockHttpClientFactory(HttpClient client)
    {
        var mock = new Mock<IHttpClientFactory>();
        mock
        .Setup(factory => factory.Create(It.IsAny<IHttpClientFactory.HttpClientFactoryConfiguration>()))
        .Returns(client);
        return mock;
    }

    private static Mock<HttpMessageHandler> MockHttpMessageHandler(string testJsonResponse)
    {
        var mockMessageHandler = new Mock<HttpMessageHandler>();
        mockMessageHandler
            .Protected()
            .Setup<Task<HttpResponseMessage>>(
                "SendAsync",
                ItExpr.IsAny<HttpRequestMessage>(),
                ItExpr.IsAny<CancellationToken>())
            .ReturnsAsync(new HttpResponseMessage
            {
                StatusCode = HttpStatusCode.OK,
                Content = new StringContent(testJsonResponse)
            });
        return mockMessageHandler;
    }
}