using System;
using Meadow;
using Meadow.Foundation.Displays;
using Meadow.Foundation.Graphics;
using Meadow.Hardware;
using Microsoft.Extensions.DependencyInjection;
using WeatherWilly.Common.Configuration;
using WeatherWilly.Common.Extensions;
using WeatherWilly.Common.IoC.Modules;

namespace WeatherWilly.Embedded.Ioc.Modules
{
    public class EmbeddedDisplayIocModule : IIocModule
    {
        public IIocModule Register(IServiceCollection serviceCollection)
        {
            serviceCollection
                .AddSingleton(CreateDisplay)
                .AddSingleton<IGraphicsDisplay>(services => services.GetRequiredService<Ssd1680>());
            return this;
        }

        private static Ssd1680 CreateDisplay(IServiceProvider services)
        {
            var config = services.GetRequiredService<IDisplayReadSettings>();
            var device = services.GetRequiredService<IMeadowDevice>();
            return new Ssd1680(
                   spiBus: services.GetRequiredService<ISpiBus>(),
                   chipSelectPin: device.GetPinSafe(config.PinSpiChipSelect),
                   dcPin: device.GetPinSafe(config.PinDataCommand),
                   resetPin: device.GetPinSafe(config.PinReset),
                   busyPin: device.GetPinSafe(config.PinBusy),
                   width: config.Width,
                   height: config.Height
            );
        }
    }
}