using System;
using Meadow;
using Meadow.Hardware;
using Meadow.Units;
using Microsoft.Extensions.DependencyInjection;
using WeatherWilly.Common.Configuration;
using WeatherWilly.Common.Extensions;
using WeatherWilly.Common.IoC.Modules;

namespace WeatherWilly.Native.IoC.Modules;

public class EmbeddedCommonIocModule : IIocModule
{
    private readonly IMeadowDevice _meadowDevice;

    public EmbeddedCommonIocModule(IMeadowDevice device)
    {
        this._meadowDevice = device;
    }

    public IIocModule Register(IServiceCollection serviceCollection)
    {
        serviceCollection
            .AddSingleton<IMeadowDevice>(this._meadowDevice)
            .AddSingleton<ISpiBus>(CreateSpiBus);
        return this;
    }

    private static ISpiBus CreateSpiBus(IServiceProvider services)
    {
        var device = services.GetRequiredService<IMeadowDevice>();
        var config = services.GetRequiredService<IDisplayReadSettings>();
        try
        {
            return device.CreateSpiBus(
                device.GetPinSafe(config.PinSpiClock),
                device.GetPinSafe(config.PinSpiMosi),
                device.GetPinSafe(config.PinSpiMiso),
                new Frequency(config.SpiFrequencyHz, Frequency.UnitType.Hertz)
            );
        }
        catch (Exception ex)
        {
            throw new Exception("Unable to creat eSPI bus", ex);
        }
    }
}
