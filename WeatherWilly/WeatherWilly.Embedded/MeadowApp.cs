﻿using Meadow;
using Meadow.Devices;
using Meadow.Foundation.Leds;
using Meadow.Gateway.WiFi;
using Meadow.Hardware;
using Meadow.Logging;
using Microsoft.Extensions.DependencyInjection;
using System;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using WeatherWilly.Common.IoC.Modules;
using WeatherWilly.Common.Service;
using WeatherWilly.Common.Weather;
using WeatherWilly.Embedded.Ioc.Modules;
using WeatherWilly.Native.IoC.Modules;

namespace MeadowApp
{
    public class MeadowApp : App<F7FeatherV1>
    {
        private IWeatherWillyService _weatherWilly;
        private IServiceProvider _serviceProvider;
        private IDelayService _delayService;
        private Logger _logger;

        public override async Task Initialize()
        {
            this._serviceProvider = new RootIocModule().Register(
                new CommonIocModule(),
                new EmbeddedCommonIocModule(Device),
                new CommonDisplayIocModule(),
                new EmbeddedDisplayIocModule(),
                new WeatherWillyIocModule()
            ).Finalize();

            Resolver.Log.Info("Initialize...");
            this._weatherWilly = this._serviceProvider.GetRequiredService<IWeatherWillyService>();
            this._delayService = this._serviceProvider.GetRequiredService<IDelayService>();
            this._logger = Resolver.Log;

            //await AwaitNetworakConnectionAsync(Device, TimeSpan.FromSeconds(60));

            await base.Initialize();
        }


        private void NetworkConnected(
               INetworkAdapter networkAdapter,
               NetworkConnectionEventArgs networkConnectionEventArgs
           )
        {
            this._logger.Info("Joined network");
            this._logger.Info($"IP Address: {networkAdapter.IpAddress}.");
            this._logger.Info($"Subnet mask: {networkAdapter.SubnetMask}");
            this._logger.Info($"Gateway: {networkAdapter.Gateway}");
        }

        public override async Task Run()
        {
            this._logger.Info("Run...");

            // for (int i = 0; i < 30; i++)
            // {
            await _weatherWilly.RunWeatherWillyCycleAsync();
            // }

            //await this._weatherService.GetPersonalWeatherStationCurrentObservationAsync();
            // await CycleColors(TimeSpan.FromMilliseconds(1000));

            this._logger.Info("Done...");
        }

        async Task AwaitNetworakConnectionAsync(
            IMeadowDevice device,
            TimeSpan timeout,
            CancellationToken cancellationToken = default
        )
        {
            var monitoringInterval = TimeSpan.FromSeconds(1);
            while (timeout.TotalMilliseconds >= 0)
            {
                var connected = device.NetworkAdapters.Any(n => n.IsConnected);
                this._logger.Info($"[{timeout.TotalMilliseconds}] Connected: {connected}...");

                if (connected)
                {
                    return;
                }
                await this._delayService.DelayAsync(monitoringInterval, cancellationToken);
                timeout -= monitoringInterval;
            }

            throw new NetworkNotFoundException("To proceed a network connection is necessary, is wifi configured?");
        }

        async Task ScanForAccessPoints(IWiFiNetworkAdapter wifi)
        {
            Console.WriteLine("Getting list of access points.");
            var networks = await wifi.Scan();

            if (networks.Count > 0)
            {
                Console.WriteLine("|-------------------------------------------------------------|---------|");
                Console.WriteLine("|         Network Name             | RSSI |       BSSID       | Channel |");
                Console.WriteLine("|-------------------------------------------------------------|---------|");

                foreach (WifiNetwork accessPoint in networks)
                {
                    Console.WriteLine($"| {accessPoint.Ssid,-32} | {accessPoint.SignalDbStrength,4} | {accessPoint.Bssid,17} |   {accessPoint.ChannelCenterFrequency,3}   |");
                }
            }
            else
            {
                Console.WriteLine($"No access points detected.");
            }
        }

    }
}