namespace WeatherWilly.Common.Service
{
    public interface IHttpClientFactory
    {
        public delegate void HttpClientFactoryConfiguration(HttpClient client);
        HttpClient Create(HttpClientFactoryConfiguration? config = null);
    }
}