namespace WeatherWilly.Common.Service;

public interface IWeatherWillyService
{
    Task RunWeatherWillyCycleAsync();
}
