using System.ComponentModel.DataAnnotations;
using Meadow.Logging;

namespace WeatherWilly.Common.Service;

public interface IDelayService
{
    Task DelayAsync(TimeSpan timeSpan, CancellationToken cancellationToken = default);
}

public class DelayService : IDelayService
{
    private readonly Logger logger;

    public DelayService(Logger logger)
    {
        this.logger = logger;
    }

    public async Task DelayAsync(TimeSpan timeSpan, CancellationToken cancellationToken = default)
    {
        EnsureValidTimeSpan(timeSpan);

        var waitUntil = DateTime.UtcNow + timeSpan;
        var fullSeconds = Math.Floor(timeSpan.TotalSeconds);

        // we delay the task in a loop so that the thread is never blocked for too long
        // this is to keep the watchdog happy and should prevent watchdog exceptions in the long run
        // also this allows us to do some progress reporting on long running delays
        for (var i = 0; i < fullSeconds && !cancellationToken.IsCancellationRequested; i++)
        {
            await Task.Delay(1000, cancellationToken);
            this.logger.Info($"Delaying {timeSpan.TotalSeconds}s until {waitUntil}: {(i + 1) / fullSeconds * 100}%");
        }

        var remainingTime = waitUntil - DateTime.UtcNow;
        if (remainingTime.TotalMilliseconds <= 0)
        {
            // execution took longer than anticipated
            return;
        }

        await Task.Delay(remainingTime);
    }

    private static void EnsureValidTimeSpan(TimeSpan timeSpan) => EnsureValidDelay(timeSpan.TotalMilliseconds);

    private static void EnsureValidDelay(double totalMilliseconds)
    {
        if (!double.IsFinite(totalMilliseconds))
        {
            throw new ArgumentException($"Unable to delay infinite milliseconds!", nameof(totalMilliseconds));
        }

        if (double.IsNaN(totalMilliseconds))
        {
            throw new ArgumentException($"Unable to delay NaN milliseconds!", nameof(totalMilliseconds));
        }

        if (totalMilliseconds < 0)
        {
            throw new ArgumentException(
                $"Unable to delay {totalMilliseconds} milliseconds! Please pick a positive finite number.",
                nameof(totalMilliseconds)
            );
        }
    }
}