using Meadow.Foundation.Graphics;
using Meadow.Logging;
using WeatherWilly.Common.Configuration;
using WeatherWilly.Common.Display;
using WeatherWilly.Common.Weather;

namespace WeatherWilly.Common.Service;

public class WeatherWillyService : IWeatherWillyService
{
    private readonly Logger _logger;
    private readonly IDelayService _delayService;
    private readonly IWundergroundApi _weatherApi;
    private readonly IWeatherWillyScene _weatherWillyScene;
    private readonly IDisplayReadSettings _displaySettings;
    private readonly IWeatherApiReadSettings _weatherApiSettings;


    public WeatherWillyService(
        Logger logger,
        IWeatherApiReadSettings weatherApiSettings,
        IDisplayReadSettings displaySettings,
        IDelayService delayService,
        IWundergroundApi weatherApi,
        IWeatherWillyScene weatherWillyScene
    )
    {
        _logger = logger;
        _delayService = delayService;
        _weatherApi = weatherApi;
        _weatherWillyScene = weatherWillyScene;
        _weatherApiSettings = weatherApiSettings;
        _displaySettings = displaySettings;
    }
    public async Task RunWeatherWillyCycleAsync()
    {
        _logger.Info($"Invoking Weather willy cycle: {DateTime.UtcNow}!");

        var weather = await _weatherApi.GetPersonalWeatherStationCurrentObservationAsync();

        var observation = weather.Observations.FirstOrDefault();
        if (observation == null)
        {
            _logger.Warn("No observations received, skipping cycle...");
            return;
        }

        _logger.Info(observation.ToString());

        _weatherWillyScene.UpdateTemperature(observation.Imperial.Temperature);
        _weatherWillyScene.Render();
    }
}