using Meadow.Logging;

namespace WeatherWilly.Common.Service;

public class NoopDelayService : IDelayService
{
    private readonly Logger _logger;

    public NoopDelayService(Logger logger)
    {
        this._logger = logger;
    }

    public Task DelayAsync(TimeSpan timeSpan, CancellationToken cancellationToken = default)
    {
        this._logger.Info($"Noop delay of {timeSpan}. Continuing without delay!");
        return Task.CompletedTask;
    }
}
