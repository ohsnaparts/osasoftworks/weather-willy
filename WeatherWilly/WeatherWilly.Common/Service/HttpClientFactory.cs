namespace WeatherWilly.Common.Service
{

    public class HttpClientFactory : IHttpClientFactory
    {
        private readonly IHttpClientFactory.HttpClientFactoryConfiguration? _baseConfiguration;

        public HttpClientFactory(IHttpClientFactory.HttpClientFactoryConfiguration? baseConfiguration = null)
        {
            this._baseConfiguration = baseConfiguration;
        }
        
        public HttpClient Create(IHttpClientFactory.HttpClientFactoryConfiguration? config = null)
        {
            var httpClient = new HttpClient();
            this._baseConfiguration?.Invoke(httpClient);
            config?.Invoke(httpClient);
            return httpClient;
        }
    }
}