using Meadow;

namespace WeatherWilly.Common.Configuration;

public class SceneSettings : ConfigurableObject, ISceneReadSettings
{
    /**
     * Resource path
     */
    public string BackgroundImagePath => GetConfiguredString(nameof(BackgroundImagePath), string.Empty);
    public bool DebugMode => GetConfiguredBool(nameof(DebugMode), false);
}
