namespace WeatherWilly.Common.Configuration
{
    public interface IWeatherApiReadSettings
    {
        public TimeSpan UpdateInterval { get; }
        public string ApiKey { get; }
        public string WeatherStationId { get; }
    }
}