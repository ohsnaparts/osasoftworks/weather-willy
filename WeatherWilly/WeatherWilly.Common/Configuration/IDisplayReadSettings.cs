namespace WeatherWilly.Common.Configuration;

public interface IDisplayReadSettings
{
    int Width { get; }
    int Height { get; }
    bool SupportsColor { get; }
    int SpiFrequencyHz { get; }
    string PinSpiChipSelect { get; }
    string PinSpiClock { get; }
    string PinSpiMiso { get; }
    string PinSpiMosi { get; }
    string PinDataCommand { get; }
    string PinReset { get; }
    string PinBusy { get; }
    string PinDisplaySramChipSelect { get; }
    
    TimeSpan MinSafeRenderInterval { get; }
    TimeSpan FullRefreshInterval { get; }
    
}
