using Meadow;

namespace WeatherWilly.Common.Configuration
{
    public class WeatherApiSettings : ConfigurableObject, IWeatherApiReadSettings
    {
        /// <summary>
        /// The API update interval / the time necessary for it to be able to provide the next set of data.
        /// Some APIs only have a data resolution of 1 minute, during which the same records are returned.
        /// </summary>
        public double UpdateIntervalSeconds => GetConfiguredFloat(nameof(UpdateIntervalSeconds), 0.0f);
        public string ApiKey => GetConfiguredString(nameof(ApiKey), string.Empty);
        public string WeatherStationId => GetConfiguredString(nameof(WeatherStationId), string.Empty);
        public TimeSpan UpdateInterval => TimeSpan.FromSeconds(this.UpdateIntervalSeconds);
    }
}