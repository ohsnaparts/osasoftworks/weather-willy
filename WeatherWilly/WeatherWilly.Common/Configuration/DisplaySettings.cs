using Meadow;

namespace WeatherWilly.Common.Configuration;

public class DisplaySettings : ConfigurableObject, IDisplayReadSettings
{
    public int Width => GetConfiguredInt(nameof(Width), 0);
    public int Height => GetConfiguredInt(nameof(Height), 0);
    /// <summary>
    /// Is the display a color display or does it support black and white only?
    /// </summary>
    public bool SupportsColor => GetConfiguredBool(nameof(SupportsColor), false);
    /// <summary>
    /// The amount of time the display needs to render 1 full image
    /// </summary>
    public double FullRefreshIntervalSeconds => GetConfiguredFloat(nameof(FullRefreshIntervalSeconds), 0);
    /// <summary>
    /// The interval during which it is safe to do full screen renderings
    /// For instance, some e-paper displays have a restriction to only render
    /// once every 3 minutes.
    /// </summary>
    public double MinSafeRenderIntervalSeconds => GetConfiguredFloat(nameof(MinSafeRenderIntervalSeconds), 0);
    
    public int SpiFrequencyHz => GetConfiguredInt(nameof(SpiFrequencyHz), 0);
    public string PinSpiChipSelect => GetConfiguredString(nameof(PinSpiChipSelect), string.Empty);
    public string PinSpiClock => GetConfiguredString(nameof(PinSpiClock), string.Empty);
    public string PinSpiMiso => GetConfiguredString(nameof(PinSpiMiso), string.Empty);
    public string PinSpiMosi => GetConfiguredString(nameof(PinSpiMosi), string.Empty);
    public string PinDataCommand => GetConfiguredString(nameof(PinDataCommand), string.Empty);
    public string PinReset => GetConfiguredString(nameof(PinReset), string.Empty);
    public string PinBusy => GetConfiguredString(nameof(PinBusy), string.Empty);
    public string PinDisplaySramChipSelect => GetConfiguredString(nameof(PinDisplaySramChipSelect), string.Empty);

    public TimeSpan MinSafeRenderInterval => TimeSpan.FromSeconds(this.MinSafeRenderIntervalSeconds);

    public TimeSpan FullRefreshInterval => TimeSpan.FromSeconds(this.FullRefreshIntervalSeconds);
}
