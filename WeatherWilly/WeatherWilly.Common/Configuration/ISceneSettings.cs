namespace WeatherWilly.Common.Configuration;

public interface ISceneReadSettings
{
    public string BackgroundImagePath { get; }
    public bool DebugMode { get; }
}
