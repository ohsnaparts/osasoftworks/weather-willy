using WeatherWilly.Common.Weather.Models;
using System.Text.Json.Serialization;

namespace WeatherWilly.Common.Weather;

[JsonSerializable(typeof(ObservationResponseDto))]
internal partial class SourceGenerationContext : JsonSerializerContext { }
