using System.Text.Json.Serialization;

namespace WeatherWilly.Common.Weather.Models;

public class ObservationResponseDto
{
    [JsonPropertyName("observations")] public ObservationDto[] Observations { get; set; } = [];

    // public override string ToString() => $"Observations (Count {this.Observations.Length})" + base.ToString();
}