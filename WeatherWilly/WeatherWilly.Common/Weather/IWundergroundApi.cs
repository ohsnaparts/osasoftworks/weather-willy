using WeatherWilly.Common.Weather.Models;

namespace WeatherWilly.Common.Weather;

public interface IWundergroundApi
{
    public Task<ObservationResponseDto> GetPersonalWeatherStationCurrentObservationAsync(
        CancellationToken cancellationToken = default);
}