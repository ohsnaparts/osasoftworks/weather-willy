﻿using System.Text.Json;
using WeatherWilly.Common.Weather.Models;
using WeatherWilly.Common.Configuration;
using Meadow.Logging;
using WeatherWilly.Common.Service;
using WeatherWilly.Common.Extensions;

namespace WeatherWilly.Common.Weather;

public class WundergroundApi : IWundergroundApi
{
    private static readonly string ApiBaseUrl = "https://api.weather.com";
    private readonly IHttpClientFactory _httpClientFactory;

    private readonly IWeatherApiReadSettings _weatherApiSettings;
    private readonly Logger _logger;

    private string _apiUrlPersonalWeatherStationCurrentObservation => $"{ApiBaseUrl}/v2/pws/observations/current?stationId={this._weatherApiSettings.WeatherStationId}&format=json&units=e&apiKey={this._weatherApiSettings.ApiKey}";

    public WundergroundApi(
        IHttpClientFactory httpClientFactory,
        IWeatherApiReadSettings weatherApiSettings,
        Logger logger
    )
    {
        this._httpClientFactory = httpClientFactory;
        this._weatherApiSettings = weatherApiSettings;
        this._logger = logger;
    }

    public async Task<ObservationResponseDto> GetPersonalWeatherStationCurrentObservationAsync(
        CancellationToken cancellationToken = default
    )
    {
        var response = await InvokeGetPersonalWeatherStationCurrentObservationRequestAsync(cancellationToken);
        this._logger.Info($"Received: {response.StatusCode}");
        response.EnsureSuccessStatusCode();

        var responseBody = await response.Content.ReadAsStringAsync();
        this._logger.Info($"Received: {responseBody}");

        var responseDto = Parse(responseBody);
        return responseDto!;
    }

    private ObservationResponseDto Parse(string responseBody)
    {
        // Meadow does assembly trimming to slim down the application by removing unused code
        // so that it fits the tiny microcontroller. This interferes with reflection based
        // code such as System.Text.Json's Deserialize method
        //     https://github.com/dotnet/runtime/issues/82761
        // As a workaround we can switch from reflection based serialization to soure-generation
        // This allows the json serializer to have static compile time information on the types to
        // handle.
        //     https://learn.microsoft.com/en-us/dotnet/standard/serialization/system-text-json/reflection-vs-source-generation?source=recommendations&pivots=dotnet-8-0
        var typeInfo = SourceGenerationContext.Default.ObservationResponseDto;
        var model = JsonSerializer.Deserialize(responseBody, typeInfo);
        model.ThrowIfNull(nameof(model));
        return model!;
    }

    private async Task<HttpResponseMessage> InvokeGetPersonalWeatherStationCurrentObservationRequestAsync(
        CancellationToken cancellationToken
    )
    {
        // Meadow is facing memory leak issues with HttpClients which makes 
        // requests incresingly unsustainable (1 request, 3-5 minutes)
        // To mitigate this memory leak, the http client needs to be disposed off
        // as soon as possible
        // https://community.wildernesslabs.co/t/problem-with-repetative-http-request/1622
        // https://github.com/WildernessLabs/Meadow_Issues/issues/286
        // https://github.com/WildernessLabs/Meadow_Issues/issues/216
        using var httpClient = this._httpClientFactory.Create();
        this._logger.Info($"GET {_apiUrlPersonalWeatherStationCurrentObservation}");
        return await httpClient.GetAsync(_apiUrlPersonalWeatherStationCurrentObservation, cancellationToken);
    }
}