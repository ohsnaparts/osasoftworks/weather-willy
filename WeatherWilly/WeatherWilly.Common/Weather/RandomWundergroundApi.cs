using WeatherWilly.Common.Weather.Models;

namespace WeatherWilly.Common.Weather;

public class RandomWundergroundApi : IWundergroundApi
{
    public Task<ObservationResponseDto> GetPersonalWeatherStationCurrentObservationAsync(CancellationToken cancellationToken = default) => Task.FromResult(new ObservationResponseDto
    {
        Observations = [this.GetRandomObservationDto()]
    });

    private ObservationDto GetRandomObservationDto()
    {
        var random = new Random();
        return new ObservationDto
        {
            ObservationTimeLocal = DateTime.Now.ToString(),
            ObservationTimeUtc = DateTime.UtcNow.ToString(),
            Humidity = random.Next() % 100,
            Imperial = new ImperialDto
            {
                Temperature = random.Next() % 40
            }
        };
    }
}
