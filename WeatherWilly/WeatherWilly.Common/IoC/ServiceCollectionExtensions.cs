using Meadow;
using Microsoft.Extensions.DependencyInjection;
using WeatherWilly.Common.Exceptions;
using WeatherWilly.Common.IoC;
using WeatherWilly.Common.IoC.MinIoC;

public static class ServiceCollectionExtensions
{
    public static IServiceCollection RegisterSettings<TInterface, TOption>(
        this IServiceCollection collection
    ) where TInterface : class
      where TOption : class, TInterface, new()
    {
        EnsureMeadowCompatibleSetting<TOption>();
        return collection.AddSingleton<TInterface>(new TOption());
    }

    private static void EnsureMeadowCompatibleSetting<TOption>()
    {
        EnsureValidSettingsName<TOption>();
        EnsureConfigurableObject<TOption>();
    }

    private static void EnsureConfigurableObject<TOption>()
    {
        var type = typeof(TOption);
        var parentType = typeof(ConfigurableObject);
        if (!type.IsSubclassOf(parentType))
        {
            throw new ConfigurationException(
                $"For the option {type.FullName} to work with Meadow, it needs to be a subclass of {parentType.FullName}");
        }
    }

    public static IServiceCollection UsingRegistrator(
        this IServiceCollection collection,
        Action<IServiceCollection> registrator
    )
    {
        registrator(collection);
        return collection;
    }

    public static IServiceCollection RegisterWithContainer(this IServiceCollection collection, Container container)
    {
        container.FromServiceCollection(collection);
        return collection;
    }

    private static void EnsureValidSettingsName<TOption>(
        string requiredSuffix = "Settings"
    )
    {
        var className = typeof(TOption).FullName;
        var hasValidSuffix = className.EndsWith(requiredSuffix);
        if (!hasValidSuffix)
        {
            throw new ConfigurationException(
                $"Unable to register settings of type <{className}>. Settings need to end with the suffix '{requiredSuffix}'"
            );
        }
    }
}