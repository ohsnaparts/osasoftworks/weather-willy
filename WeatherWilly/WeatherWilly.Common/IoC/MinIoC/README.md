# MinIoC

MinIoC is a single-file, minimal C# IoC container. While there are several great IoC solutions available which are much more powerful and flexible, MinIoC aims to enable lightweight/small-footprint projects with a simple implementation. It is distributed as a single .cs file which can be included and compiled in other projects.

## Source

https://github.com/microsoft/MinIoC