using WeatherWilly.Common.IoC.MinIoC;
using static WeatherWilly.Common.IoC.MinIoC.Container;

namespace WeatherWilly.Common.IoC;

public static class ContainerExtensions
{
    public static IRegisteredType Register<TInterface>(
        this Container container,
        object value
    ) => container.Register<TInterface>(() => value);

    public static IRegisteredType Register<TInterface>(
        this Container container,
        Func<object> factory
    ) => container.Register<TInterface>((_) => factory());

    public static IRegisteredType Register<TInterface>(
        this Container container,
        Func<IServiceProvider, object> factory
    )
    {
        container.Log($"Registering result of factory as {typeof(TInterface).FullName}.");
        return container.Register(typeof(TInterface), () => factory(container));
    }

    public static Container UsingRegistrator(this Container container, Action<Container> registrator) {
        registrator(container);
        return container;
    }
}