using WeatherWilly.Common.IoC.MinIoC;
using Microsoft.Extensions.DependencyInjection;
using static WeatherWilly.Common.IoC.MinIoC.Container;
using WeatherWilly.Common.Extensions;

namespace WeatherWilly.Common.IoC;

public static class ContainerServiceCollectionExtensions
{
    public static Container FromServiceCollection(this Container container, IServiceCollection serviceCollection)
    {
        container.Log($"Registering services from service collection ({serviceCollection.Count} services).");
        container.Register<IServiceCollection>(serviceCollection).AsSingleton();
        foreach (var serviceDescriptor in serviceCollection)
        {
            container.Register(serviceDescriptor);
        }

        return container;
    }

    internal static Exception NewUnsupportedServiceLifeTimeException(ServiceLifetime serviceLifetime) => new Exception(
        $"{nameof(ServiceLifetime)} {Enum.GetName(typeof(ServiceLifetime), serviceLifetime)} is currently unsupported"
    );

    internal static IRegisteredType Register<TInterface, TImplementation>(
            this Container container,
            ServiceLifetime lifetime
        ) where TImplementation : TInterface => container.Register(
            typeof(TInterface),
            typeof(TImplementation),
            lifetime
        );

    private static IRegisteredType Register(this Container container, ServiceDescriptor descriptor)
    {
        var isInstanceRegistration = descriptor.ImplementationInstance != null;
        if (isInstanceRegistration)
        {
            return container.Register(
                descriptor.ServiceType,
                descriptor.ImplementationInstance!,
                descriptor.Lifetime
            );
        }

        if (descriptor.ServiceType == null)
        {
            throw new ArgumentNullException(nameof(descriptor.ServiceType));
        }

        var isTypeRegistration = descriptor.ImplementationType != null;
        if (isTypeRegistration)
        {
            return container.Register(
                descriptor.ServiceType,
                descriptor.ImplementationType!,
                descriptor.Lifetime);
        }

        var isFactoryRegistation = descriptor.ImplementationFactory != null;
        if (isFactoryRegistation)
        {
            return container.Register(
                descriptor.ServiceType,
                descriptor.ImplementationFactory!,
                descriptor.Lifetime
            );
        }

        throw new NotImplementedException("Unable to register service path due to it not being implemented yet.");
    }

    private static IRegisteredType Register(
        this Container container,
        Type interfaceType,
        Type implementationType,
        ServiceLifetime lifetime
    )
    {
        var lifetimeName = Enum.GetName(typeof(ServiceLifetime), lifetime);
        container.Log($"Registering {lifetimeName} {implementationType.FullName} as {interfaceType.FullName}.");

        return container
            .Register(interfaceType, implementationType)
            .WithLifeTime(lifetime);
    }

    private static IRegisteredType Register(
        this Container container,
        Type serviceType,
        Func<IServiceProvider, object> factory,
        ServiceLifetime lifetime
    )
    {
        container.Log($"Registering {lifetime.Name()} factory as {serviceType.FullName}.");
        return container
            .Register(serviceType, () => factory(container))
            .WithLifeTime(lifetime);
    }

    private static IRegisteredType Register(
        this Container container,
        Type serviceType,
        object instance,
        ServiceLifetime lifetime
    )
    {
        container.Log($"Registering {lifetime.Name()} {instance.GetType().FullName} as {serviceType.FullName}.");
        return container.Register(serviceType, () => instance).WithLifeTime(lifetime);
    }

    private static IRegisteredType WithLifeTime(this IRegisteredType registeredType, ServiceLifetime lifetime)
    {
        switch (lifetime)
        {
            case ServiceLifetime.Singleton:
                {
                    registeredType.AsSingleton();
                    return registeredType;
                }
            case ServiceLifetime.Scoped:
                {
                    registeredType.PerScope();
                    return registeredType;
                }
            case ServiceLifetime.Transient:
                {
                    return registeredType;
                }
        };

        throw NewUnsupportedServiceLifeTimeException(lifetime);
    }

}
