using WeatherWilly.Common.IoC.MinIoC;
using Microsoft.Extensions.DependencyInjection;
using ServiceCollection = Microsoft.Extensions.DependencyInjection.ServiceCollection;


namespace WeatherWilly.Common.IoC.Modules;

public class RootIocModule
{
    private readonly Container _container;
    private readonly IServiceCollection _serviceCollection;

    public RootIocModule()
    {
        this._container = new Container();
        this._serviceCollection = new ServiceCollection();
    }

    public RootIocModule Register(params IIocModule[] modules)
    {
        this._serviceCollection
            .AddSingleton<Container>(this._container)
            .AddTransient<IServiceProvider, Container>();

        foreach (var module in modules)
        {
            module.Register(this._serviceCollection);
        }

        return this;
    }

    public IServiceProvider Finalize() => this._container.FromServiceCollection(this._serviceCollection);
}
