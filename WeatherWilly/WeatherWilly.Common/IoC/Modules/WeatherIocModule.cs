using WeatherWilly.Common.Configuration;
using Microsoft.Extensions.DependencyInjection;
using WeatherWilly.Common.Weather;
using WeatherWilly.Common.Service;

namespace WeatherWilly.Common.IoC.Modules;

public class WeatherWillyIocModule : IIocModule
{
    public IIocModule Register(IServiceCollection serviceCollection)
    {
        serviceCollection
            .RegisterSettings<IWeatherApiReadSettings, WeatherApiSettings>()
            .AddSingleton<IWeatherWillyService, WeatherWillyService>()
            .AddSingleton<IWundergroundApi, RandomWundergroundApi>();
        return this;
    }
}
