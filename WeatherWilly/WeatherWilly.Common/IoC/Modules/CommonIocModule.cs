using WeatherWilly.Common.Configuration;
using Meadow;
using Meadow.Logging;
using Microsoft.Extensions.DependencyInjection;
using WeatherWilly.Common.Service;

namespace WeatherWilly.Common.IoC.Modules;

public class CommonIocModule : IIocModule
{
    public IIocModule Register(IServiceCollection serviceCollection)
    {
        this.RegisterSettings(serviceCollection)
            .AddSingleton<Logger>(_ => Resolver.Log)
            .AddSingleton<IMeadowDevice>(_ => Resolver.Device)
            .AddTransient<IDelayService, DelayService>()
            .AddSingleton<IHttpClientFactory>(_ => new HttpClientFactory(client =>
            {
                // some APIs hard require a valid user-agent header => impersonate known browser
                const string firefox = "Mozilla/5.0 (X11; Linux x86_64; rv:120.0) Gecko/20100101 Firefox/120.0";
                client.DefaultRequestHeaders.Add("User-Agent", firefox);
            }));
        return this;
    }

    private IServiceCollection RegisterSettings(IServiceCollection serviceCollection) => serviceCollection
        .RegisterSettings<IDisplayReadSettings, DisplaySettings>()
        .RegisterSettings<ISceneReadSettings, SceneSettings>();

}