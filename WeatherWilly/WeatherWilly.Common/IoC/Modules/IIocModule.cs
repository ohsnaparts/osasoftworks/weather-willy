using Microsoft.Extensions.DependencyInjection;

namespace WeatherWilly.Common.IoC.Modules;

public interface IIocModule {
    IIocModule Register(IServiceCollection serviceCollection);
}