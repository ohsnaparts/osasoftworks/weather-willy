using Meadow.Foundation.Graphics;
using Microsoft.Extensions.DependencyInjection;
using WeatherWilly.Common.Display;
using WeatherWilly.Common.IoC.Modules;

public class CommonDisplayIocModule : IIocModule
{
    public IIocModule Register(IServiceCollection serviceCollection)
    {
        serviceCollection
            .AddSingleton<MicroGraphics>(BuildMicroGraphics)
            .AddSingleton<IWeatherWillyScene, WillyDisplayScene>();
        return this;
    }

    private static MicroGraphics BuildMicroGraphics(IServiceProvider services) => new MicroGraphics(
        services.GetRequiredService<IGraphicsDisplay>()
    )
    {
        IgnoreOutOfBoundsPixels = true
    };
}