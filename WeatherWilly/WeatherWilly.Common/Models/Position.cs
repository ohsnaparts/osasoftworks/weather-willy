using WeatherWilly.Common.Display;

namespace WeatherWilly.Common.Models;

public class Position : Vector3D<int>
{
    public Position(int x = 0, int y = 0, int z = 0) : this((x, y, z))
    {
    }

    public Position((int x, int y, int z) position) : base(position)
    {
    }

    public static Position Zero => new(0, 0, 0);
}