namespace WeatherWilly.Common.Models;

public class Vector3D<T> where T : struct
{
    private (T x, T y, T z) _position;

    public Vector3D(
        T x = default,
        T y = default,
        T z = default
    )
    {
    }

    public Vector3D((T x, T y, T z) position)
    {
        _position = position;
    }

    public T X
    {
        get => _position.x;
        set => _position.x = value;
    }
    public T Y
    {
        get => _position.y;
        set => _position.y = value;
    }
    public T Z
    {
        get => _position.z;
        set => _position.z = value;
    }
}