namespace WeatherWilly.Common.Models;

public class Size : Vector3D<int>
{
    public Size(int x = 0, int y = 0, int z = 0) : this((x, y, z))
    {
    }

    public Size((int x, int y, int z) position) : base(position)
    {
    }
}
