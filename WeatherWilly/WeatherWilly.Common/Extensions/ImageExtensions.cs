using System.Text;
using Meadow.Foundation;
using Meadow.Foundation.Graphics;
using Meadow.Logging;

namespace MeadowDesktopGtk.Display.Extensions
{
    public static class ImageExtensions
    {
        public static void PrintAsAscii(this Image image, Logger logger, bool invert = false)
        {
            logger.Info($"ASCII representation of image {image.Width}px{image.Width}px/{image.BitsPerPixel}bit:");
            var charByColor = new Dictionary<Color, char>();
            var currentLine = -1;
            var lineBuilder = new StringBuilder(image.DisplayBuffer?.Width ?? 0);

            image.ForEachPixel((x, y, pixel) =>
            {
                var lineChanged = currentLine != y;
                if (lineChanged)
                {
                    currentLine = y;
                    logger.Info($"{y:000}: {lineBuilder}");
                    lineBuilder.Clear();
                }

                if (!charByColor.TryGetValue(pixel, out var pixelChar))
                {
                    pixelChar = ColorToCharacter(pixel, invert);
                    charByColor.Add(pixel, pixelChar);
                }

                lineBuilder.Append(pixelChar);
            });

            PrintColorIndex(charByColor, logger);
        }

        public static void ForEachPixel(this Image image, Action<int, int, Color> action)
        {
            for (var y = 0; y < image.DisplayBuffer?.Height; y++)
            {
                for (var x = 0; x < image.DisplayBuffer?.Width; x++)
                {
                    var pixel = image.DisplayBuffer.GetPixel(x, y);
                    action(x, y, pixel);
                }
            }
        }

        private static void PrintColorIndex(
            IDictionary<Color, char> charByColor,
            Logger logger
        )
        {
            logger.Info("Color index:");
            foreach (var tuple in charByColor)
            {
                logger.Info($"'{tuple.Value}': {tuple.Key}");
            }
        }

        private static char ColorToCharacter(
            Color color,
            bool invert,
            char black = '.',
            char white = ' '
        )
        {
            var charColor = color.Average();
            if (invert)
            {
                charColor = (byte)(byte.MaxValue - charColor);
            }

            var isWhite = charColor == byte.MaxValue;
            if (isWhite)
            {
                return white;
            }

            var isBlack = charColor == byte.MinValue;
            if (isBlack)
            {
                return black;
            }

            return (char)(white + charColor);
        }
    }
}