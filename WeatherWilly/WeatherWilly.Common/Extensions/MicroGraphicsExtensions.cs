namespace MeadowDesktopGtk.Display.Extensions;

using System;
using Meadow.Foundation.Graphics;

public static class MicroGraphicsExtensions
{
    public static void WithinDrawingSession(this MicroGraphics graphics, Action action)
    {
        graphics.SaveState();
        try
        {
            action();
        }
        finally
        {
            graphics.RestoreState();
        }
    }
}
