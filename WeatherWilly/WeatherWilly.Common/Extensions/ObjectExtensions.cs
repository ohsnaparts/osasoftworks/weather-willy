namespace WeatherWilly.Common.Extensions
{
    public static class ObjectExtensions
    {
        public static void ThrowIfNull(this object? @object, string objName)
        {
            if (@object == null)
            {
                throw new ArgumentNullException(objName);
            }
        }
    }
}