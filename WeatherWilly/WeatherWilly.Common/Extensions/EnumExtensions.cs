namespace WeatherWilly.Common.Extensions
{
    public static class EnumExtensions
    {
        // TODO: do performance tests if caching here is validated (mind that less run time means longer battery life)
        public static string Name(this Enum value) => Enum.GetName(value.GetType(), value);
    }
}