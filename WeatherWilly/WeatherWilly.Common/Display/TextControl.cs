using Meadow.Foundation;
using Meadow.Foundation.Graphics;
using Meadow.Foundation.Graphics.MicroLayout;
using Meadow.Logging;
using MeadowDesktopGtk.Display.Extensions;
using WeatherWilly.Common.Models;
using Size = WeatherWilly.Common.Models.Size;

namespace WeatherWilly.Common.Display;

public class TextControl : IControl
{
    private readonly Logger _logger;
    private readonly Position _position;
    private readonly IFont _font;
    private readonly Color _color;
    private readonly ScaleFactor _scaleFactor;
    private string _text = string.Empty;

    public TextControl(
        Logger logger,
        Position position,
        string text,
        IFont font,
        Color color,
        ScaleFactor scaleFactor
    )
    {
        this._logger = logger;
        this._position = position;
        this._text = text;
        this._font = font;
        this._color = color;
        this._scaleFactor = scaleFactor;
    }

    public int Left { get => this._position.X; set => this._position.X = value; }
    public int Top { get => this._position.Y; set => this._position.Y = value; }
    public int Width { get => MeasureText(this._text, _font, this._scaleFactor).X; set => _ = value; }
    public int Height { get => MeasureText(this._text, _font, this._scaleFactor).Y; set => _ = value; }
    public bool Visible { get; set; } = true;
    public string Text
    {
        get => this._text;
        set
        {
            this._text = value;
            if (this._text != value)
            {
                this.Invalidate();
            }
        }
    }
    public bool IsInvalid { get; private set; } = true;

    public void Invalidate()
    {
        IsInvalid = false;
    }

    public void Refresh(MicroGraphics graphics)
    {
        if (!IsInvalid)
        {
            return;
        }

        graphics.WithinDrawingSession(() =>
        {
            graphics.CurrentFont = this._font;
            graphics.PenColor = this._color;

            this._logger.Info($"Drawing text: ${this.Text}");
            Color backgroundColor = Color.White;
            graphics.DrawRectangle(_position.X, _position.Y, this.Width, this.Height, backgroundColor, true);
            graphics.DrawText(_position.X, _position.Y, this.Text, _scaleFactor);
        });

        IsInvalid = true;
    }

    private static Size MeasureText(
        string text,
        IFont font,
        ScaleFactor scaleFactor
    ) => new Size(
        font.Width * (int)scaleFactor * text.Length,
        font.Height * (int)scaleFactor
    );
}