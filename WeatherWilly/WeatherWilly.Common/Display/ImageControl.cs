using Meadow.Foundation.Graphics;
using Meadow.Foundation.Graphics.MicroLayout;
using Meadow.Logging;
using MeadowDesktopGtk.Display.Extensions;
using WeatherWilly.Common.Models;

namespace WeatherWilly.Common.Display
{
    public class ImageControl : IControl
    {
        private readonly Logger _logger;
        private readonly Position _position;
        private readonly Image _image;

        public ImageControl(
            Logger logger,
            Position position,
            Image image
        )
        {
            _logger = logger;
            _position = position;
            _image = image;
        }

        public int Left { get => _position.X; set => _position.X = value; }
        public int Top { get => _position.Y; set => _position.Y = value; }
        public int Width { get => _image.Width; set => _ = value; }
        public int Height { get => _image.Height; set => _ = value; }
        public bool Visible { get; set; } = true;
        public bool IsInvalid { get; private set; } = true;

        public void Invalidate()
        {
            IsInvalid = false;
        }

        public void Refresh(MicroGraphics graphics)
        {
            if (!IsInvalid)
            {
                return;
            }

            this._image.PrintAsAscii(_logger, false);
            Draw(this._image, this._position, graphics);

            IsInvalid = true;
        }

        private static void Draw(Image image, Position position, MicroGraphics graphics) => image.ForEachPixel(
            (x, y, color) => graphics.DrawPixel(position.X + x, position.Y + y, color)
        );
    }
}