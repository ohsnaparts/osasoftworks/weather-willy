using Meadow.Units;

namespace WeatherWilly.Common.Display
{
    public interface IWeatherWillyScene : IDisplayScene {
        void UpdateTemperature(int celsius);
    }
}