using Meadow.Foundation.Graphics;
using Meadow.Foundation.Graphics.MicroLayout;
using Meadow.Logging;
using WeatherWilly.Common.Configuration;
using WeatherWilly.Common.Models;
using Size = WeatherWilly.Common.Models.Size;

namespace WeatherWilly.Common.Display
{
    public class WeatherWillySceneControl : IControl
    {
        private static uint BackgroundControlIndex = 0;
        private static uint WillyControlIndex = 1;
        private static uint TemperatureControlIndex = 2;

        private readonly Logger _logger;
        private readonly Position _position;
        private readonly Size _size;
        private readonly IControl[] _controls = new IControl[3];

        public WeatherWillySceneControl(
            BoxControl backgroundControl,
            TextControl temperatureTextControl,
            ImageControl willyImageControl,
            IDisplayReadSettings displaySettings,
            Logger logger
        )
        {
            _position = Position.Zero;
            _size = new Size(displaySettings.Width, displaySettings.Height);
            _logger = logger;

            _controls[BackgroundControlIndex] = backgroundControl;
            _controls[WillyControlIndex] = willyImageControl;
            _controls[TemperatureControlIndex] = temperatureTextControl;
        }

        public IControl? Parent { get; set; } = null;
        public int Left { get => _position.X; set => _position.X = value; }
        public int Top { get => _position.Y; set => _position.Y = value; }
        public int Width { get => _size.X; set => _size.X = value; }
        public int Height { get => _size.Y; set => _size.Y = value; }
        public bool Visible { get; set; } = true;

        public bool IsInvalid { get; private set; }

        public void Invalidate()
        {
            IsInvalid = true;
        }

        public void Refresh(MicroGraphics graphics)
        {
            var invalidControls = _controls.Where(c => c.IsInvalid);
            if (!invalidControls.Any())
            {
                _logger.Info("Control contents unchanged...");
                return;
            }

            foreach (var control in invalidControls)
            {
                control.Refresh(graphics);
            }

            // render only if any of the child controls changed
            _logger.Info("Re-rendering refreshed controls...");
            graphics.Show();
        }


    }
}