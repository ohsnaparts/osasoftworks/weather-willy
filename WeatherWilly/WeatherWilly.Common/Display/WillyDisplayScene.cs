using Meadow.Foundation;
using Meadow.Foundation.Graphics;
using Meadow.Logging;
using WeatherWilly.Common.Configuration;
using WeatherWilly.Common.Models;
using Size = WeatherWilly.Common.Models.Size;

namespace WeatherWilly.Common.Display
{
    public class WillyDisplayScene : IWeatherWillyScene
    {
        private readonly TextControl _temperatureTextControl;
        private readonly WeatherWillySceneControl _sceneControl;
        private readonly Logger _logger;
        private readonly MicroGraphics _graphics;

        public WillyDisplayScene(
            Logger logger,
            MicroGraphics graphics,
            ISceneReadSettings sceneSettings,
            IDisplayReadSettings displaySettings
        )
        {
            this._logger = logger;
            this._graphics = graphics;
            this._temperatureTextControl = InitTemperatureTextControl();
            this._sceneControl = new WeatherWillySceneControl(
                InitBackgroundControl(),
                this._temperatureTextControl,
                InitWillyControl(sceneSettings),
                displaySettings,
                this._logger
            );
        }

        public void UpdateTemperature(int celsius)
        {
            this._logger.Info($"Updating temeprature: ${celsius}");
            this._temperatureTextControl.Text = BuildTemperatureDisplayText(celsius);
        }

        public string BuildTemperatureDisplayText(int celsius)
        {
            const string unit = "°C";
            var sign = celsius < 0 ? "-" : "+";
            var value = $"{celsius:00}";
            return $"{sign}{value}{unit}";
        }

        public void Render()
        {
            this._sceneControl.Refresh(this._graphics);
        }

        public BoxControl InitBackgroundControl()
        {
            _logger.Info("Initializing background control..");
            return new BoxControl(
                Position.Zero,
                new Size(this._graphics.Width, this._graphics.Height),
                Color.White);
        }

        public ImageControl InitWillyControl(ISceneReadSettings sceneSettings)
        {
            var image = Image.LoadFromResource(sceneSettings.BackgroundImagePath);
            var position = new Position(0, 10);
            return new ImageControl(
                this._logger,
                position,
                image
            );
        }

        public TextControl InitTemperatureTextControl()
        {
            var position = new Position(0, 180);
            return new TextControl(
                this._logger,
                position,
                "00C",
                new Font12x20(),
                Color.Black,
                ScaleFactor.X2
            );
        }
    }
}