namespace WeatherWilly.Common.Display
{

    public interface IDisplayScene
    {
        public void Render();
    }
}