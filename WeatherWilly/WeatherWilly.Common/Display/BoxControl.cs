using Meadow.Foundation;
using Meadow.Foundation.Graphics;
using Meadow.Foundation.Graphics.MicroLayout;
using WeatherWilly.Common.Models;
using Size = WeatherWilly.Common.Models.Size;

namespace WeatherWilly.Common.Display
{
    public class BoxControl : IControl
    {
        private readonly Position position;
        private readonly Size size;

        public BoxControl(
            Position position,
            Size size,
            Color color,
            bool filled = true,
            bool visible = true
        )
        {
            this.position = position;
            this.size = size;
            Color = color;
            Filled = filled;
            Visible = visible;
        }

        public int Left { get => position.X; set => position.X = value; }
        public int Top { get => position.Y; set => position.Y = value; }
        public int Width { get => size.X; set => size.X = value; }
        public int Height { get => size.Y; set => size.Y = value; }
        public Color Color { get; }
        public bool Filled { get; }
        public bool Visible { get; set; }

        public bool IsInvalid { get; private set; } = true;

        public void Invalidate()
        {
            IsInvalid = true;
        }

        public void Refresh(MicroGraphics graphics)
        {
            if (!IsInvalid)
            {
                return;
            }

            graphics.DrawRectangle(Left, Top, Width, Height, Color, Filled);

            IsInvalid = false;
        }
    }
}