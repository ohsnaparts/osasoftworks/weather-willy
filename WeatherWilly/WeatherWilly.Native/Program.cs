﻿using Meadow;
using Meadow.Foundation.Displays;
using Meadow.Foundation.Graphics;
using Meadow.Logging;
using Meadow.Pinouts;
using Microsoft.Extensions.DependencyInjection;
using WeatherWilly.Common.Display;
using WeatherWilly.Common.IoC.Modules;
using WeatherWilly.Common.Service;
using WeatherWilly.Native.IoC.Modules;

public class MeadowApp : App<Linux<WSL2>>
{
    private CancellationToken _cancellationTokenSource = new CancellationTokenSource().Token;
    private Logger _logger = Resolver.Log;
    private GtkDisplay _display = null!;
    private IServiceProvider _serviceProvider = null!;
    private IWeatherWillyService _weatherWilly = null!;

    static async Task Main(string[] args)
    {
        await MeadowOS.Start(args);
    }

    public override async Task Initialize()
    {
        Resolver.Log.Info("Initialize...");
        this._serviceProvider = SetupIoc();
        this._weatherWilly = this._serviceProvider.GetRequiredService<IWeatherWillyService>();
        this._display = this._serviceProvider.GetRequiredService<GtkDisplay>();

        await base.Initialize();
    }

    private IServiceProvider SetupIoc()
    {
        Resolver.Log.Info("Initializing Ioc...");
        return new RootIocModule().Register(
            new CommonIocModule(),
            new WeatherWillyIocModule(),
            new CommonDisplayIocModule(),
            new NativeDisplayIocModule()
        ).Finalize();
    }

    public override async Task Run()
    {
        this._logger.Info("Run...");
        this._logger.Info("Hello, Meadow.Linux!");


        new Thread(async () =>
        {
            while (!this._cancellationTokenSource.IsCancellationRequested)
            {
                await this._weatherWilly.RunWeatherWillyCycleAsync();
                await Task.Delay(1000);
            }
        }).Start();


        this._display.Run();
        await base.Run();
    }
}