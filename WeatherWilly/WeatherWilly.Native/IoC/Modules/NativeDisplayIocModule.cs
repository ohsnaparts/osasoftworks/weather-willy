using Meadow.Foundation.Displays;
using Meadow.Foundation.Graphics;
using Microsoft.Extensions.DependencyInjection;
using WeatherWilly.Common.Configuration;
using WeatherWilly.Common.IoC.Modules;

namespace WeatherWilly.Native.IoC.Modules;

public class NativeDisplayIocModule : IIocModule
{
    public IIocModule Register(IServiceCollection serviceCollection)
    {
        serviceCollection
            .AddSingleton<GtkDisplay>(BuildDisplay)
            .AddSingleton<IGraphicsDisplay>(services => services.GetRequiredService<GtkDisplay>());
        return this;
    }

    private static GtkDisplay BuildDisplay(IServiceProvider services)
    {
        var config = services.GetRequiredService<IDisplayReadSettings>();
        return new GtkDisplay(
            config.Width,
            config.Height,
            ColorMode.Format16bppRgb565);
    }
}