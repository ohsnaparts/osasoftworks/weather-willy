$BuildConfiguration = 'Debug'.ToLower();
$ProjectDirPath = Join-Path $PSScriptRoot 'WeatherWilly.Native/'

dotnet run --configuration $BuildConfiguration --project $ProjectDirPath
