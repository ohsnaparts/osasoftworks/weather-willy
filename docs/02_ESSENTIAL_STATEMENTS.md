# Essential Statements

## Data Flow

| Actor | Action |
| ----- | ------ |
| Web UI | Configures the system such as polling intervals, update thresholds, wifi, api-keys... |
| Weather Data Provider | Online or offline provider for weather data points (sensors or public weather API) provides values to be rendered onto a screen. |
| Display | Physically renders information onto a screen |

![data flow diagram for the wather willy system. Graphic editable with excallidraw.][ww-dataflow-diagram]

These statements are derived from the [vision statement] and focus on the core functionality of the system.

1. **Information is gathered in intervals**
Information such as _temperature_  and _humidity_ are gathered in large intervals (2-5 minute).
1. **An active network connection is an opt-in feature.**
The device does not enforce an active internet connection.
1. **Board connected sensors** provide basic environmental readings and make the device fully offline capable out of the box.
1. Weather information is gathered from **online sources** to cover cloud connected **PWS** (personal weather stations) such as wunderground.
1. Text and in particular an avatar is rendered onto an on-board e-paper display in such a way that **only the parts are rendered that changed significantly**.
1. **Weather rules determine weather willis appearance** over time. Certain value combinations, trigger willi to change appearance (ie. load a new image or update parts of his body).
   - | Rule | Action |
     | ---- | -------- |
     | Sunny | Puts on sunglasses |
     | > 30deg | Bathing suit |
     | < 30deg | Street wear |
     | < 15 deg | Jacket |
     | < 0 | Willi is frozen |
1. The device keeps energy consumption as low as possible by
   - hibernating through larger waiting periods
   - updating screens only if data changes significantly
   - updates only the parts of the screen that actually changed
1. Users are able to configure the system through a mobile friendly web application.

[vision statement]: ./01_VISION.md
[ww-dataflow-diagram]: ./images/ww-data-flow.excallidraw.svg
