# Weather Willi

## Vision

Personal weather stations are providing a lot of information crammed into one technical screen. Most of the times, all you want to know is whether you should put on a rain coat or a sweat shirt.

Weather Willi's approach is more personal yet  minimalistic. A cute avatar demonstrates the need-to-know data by changing it's appearance  accordingly.

* 30 degrees: Willi wears swimming trunks
* 90% humidiy: Willi opens the umbrella
* 10 degrees: Time for winter wear

## [Non-Functional Requirements]

### Configurability

The device is configurable either via configuration file at design time or at run time. **For instance**
* update thresholds
* sleep intervals
* api key
* wifi credentials
* rule set for willy appearances
* willy appearance files

### Efficiency

The device uses the given resources in such a way to not waste energy needlessly. **For instance**
* goes to sleep while waiting for the next refresh iteration
* only renders portions of the screen affected by changes



## Use Cases / Functional Requirements

### Network Setup

Initially, the device does not yet have internet connection. Since the first stages are highly relyant on that data, the user has to be guided through setting up the wi-fi or bluetooth connection.

### API Key configuration

Given a configured network connection, the device needs to connect to an API such as [wunderground] to pull weather information from. Necessary information like API Keys are to be configured analogue to the "Network Setup"

### Weather text information

Given a fully configured device, weather information is polled in configurable intervalls (once a minute) and displayed on screen as text. Unnecessary screen renderings are minimized by re-rendering only the parts of the screen whose values have changed significantly

Significant changes can be
- predefined threshold: `floor(Δtemp) >= 1 deg`
- willi rule trigger: `snowman-willi-rule(temp) is true`

### Weather Willi

An avatar is rendered based on a specific rule set. These rules are based on weather data and trigger weather willi to change appearance. In the beginning there will only a minimal set of appearances but more will to come. The system is easily extendable with new rules and new appearances. Ideally, in the future, users are able to configure them through a web ui themselves. To prevent unnecessary display updates, and therefore a waste of energy, weather willi is re-rendered only if one of the rules trigger a change in appearance. Furthermore only the parts that actually changed are rerendered. Depending on the thresholds, this will be roughly every 10 minutes so re-rendering willi will not be a big energy concern.

### Power supply

For the first release a wired power supply is acceptable, yet for future releases, the device must be able to be powered through either standard AAA- or a rechargable battery.

### Sustainability

Since data updates are not time critical (once every 5-10 minutes), the device is to be kept as low engergy as possible. During waiting times, the device should power down or go into an energy saving mode.

### Battery state

With the device being powered by external batteries, the user needs to know when it is time to switch batteries or plug in the charger. A battery indicator displays the current percentage of battery left.

### Indoor Temperature

Willi is able to detect the indoor temperature through board connected sensors and display it aside the outdoor temperature. This is also the fallback for people not having an active internet connection, enabling them to wire an outside sensor.

## Offline use

The device is to work out of the box, without connection to the internet. An active internet connection is to be seen as optional feature that users may or may not opt-in to.

### Constraints

#### Hardware constraints

Hardware for microcontroller and displays have already been accquired.

- Microcontroller: [Meadow F7]
   * The time table is tough which means there is no room for extensive training on the job
   * The team already knows .NET Standard v2.1 quite well so it does not have to handle all the intricacies of low level embedded C code.
   * Meadow OS comes with APIs and libraries for handling most of the feature requirements out of the box
- Display: [Waveshare 2.9inch E-Paper Display]
   * E-Paper displays are power efficient due to them not requiring constant current to sustain their image. Since display images will change in very large timeframes (5-10 minutes) there's no need to integrate an "always on" display such as OLED or LCD.
- Light: [Temt6000]
   * [Meadow.Foundation] supported sensor we have used successfully in other projects
- Temperature: [LM35DZ] / [GY-21]
   * [Meadow.Foundation] supported sensor we have used successfully in other projects

[Meadow.Foundation]: https://developer.wildernesslabs.co/Meadow/Meadow.Foundation/


#### Time

* A first prototype needs to be finished by christmas  2023 (2 months)
   * This project is intended to be a gift
* Given this tough deadline, lots of compromises will have to be made that are to be corrected after the  deadline is met.
* The minimal feature set includes
   - The device is powered over USB and connected to the WIFI configuration at compile time.
   - Weather data is collected from public APIs and trigger text and avatar display updates based on a predefined set of rules.
   - Weather willi appearances must already change based on this predefined rule set yet each appearance itself can be kept very rudimentary (create 4 willis for 4 rules and render them as a whole)

#### Performance / User Experience

Since we are using e-paper displays, there are considerable refresh rates involved when writing to the screen. Each full screen update can involve up to 30 seconds of flashing the screen black and white before rendering the image fully. The display found in the "hardware constraints" is picked with the intention of enablning partial screen updates and keeping full screen refresh rates to a minimum. No hard constraints are made here, yet user experience is to be kept in mind when developing the rendering mechanism.

One negative example is a tri-color display without partial updates and a full rendering speed of 35 seconds. This means that when updating a temperature value every minute, the user receives a device that  flickers black/white for more than 50% of the time.

### Web / Mobile UI

For the _future_ it might be valuable for users to setup, configure and customize their device using a simple mobile friendly user interface.


[Meadow F7]: https://developer.wildernesslabs.co/Meadow/Meadow_Basics/Hardware/
[Waveshare 2.9inch E-Paper Display]: https://www.waveshare.com/2.9inch-e-Paper-Module.htm
[Temt6000]: https://learn.sparkfun.com/tutorials/temt6000-ambient-light-sensor-hookup-guide/all
[GY-21 HTU21]: https://www.az-delivery.de/en/products/gy-21-temperatur-sensor-modul
[Non-Functional Requirements]: https://en.wikipedia.org/wiki/Non-functional_requirement
[wunderground]: https://www.wunderground.com/weather/api