Function Install-MeadowCli {    
    dotnet tool install WildernessLabs.Meadow.CLI --global
}

Function Test-MeadowCliInstalled {

    Try {
        $MeadowCommand = Get-Command 'meadow' -ErrorAction Stop
        & $MeadowCommand --version
        return $LASTEXITCODE -eq 0
    } Catch {
        return $False
    }

    return $False
}

Function Invoke-MeadowCommand {
    [CmdletBinding()]
    param (
        [Parameter(Mandatory)]
        [scriptblock]
        $ScriptBlock
    )

    $OutputColor = @{
        ForegroundColor = [System.ConsoleColor]::Magenta
    }

    If ( -not (Test-MeadowCliInstalled)) {
        Install-MeadowCli
    }
    
    Write-Host @OutputColor "Searching meadow interfaces"
    $MeadowInterfaces = meadow list ports `
        | Select-String '/dev/.+$' -AllMatches `
        | ForEach-Object { $_.Matches.Value }

    Write-Host @OutputColor "Found $($MeadowInterfaces -join ',')"
    If ( $MeadowInterfaces.Count -gt 1 ) {
        Write-Error "More than one connected meadow device was found.
        To prevent data loss by flashing the wrong one, the process is discontinued.
        Please unplug one of them!"
    }
    
    Write-Host @OutputColor "Using meadow on port $MeadowInterfaces"
    meadow use port $MeadowInterfaces
    & $ScriptBlock
}